var express = require('express');
var router = express.Router();
// var navbarjs = require('../public/javascripts/navbar');
var bodyParser = require('body-parser');
var gameStatsModel = require('./../public/javascripts/models/gameStatsModel');
var moment = require('moment');

router.get('/game/:gameId', async function(req, res, next){
    let gameStats = await gameStatsModel.fetchGameStats(req.params.gameId);
    gameStats.game_time = moment(gameStats.game_time, 'YYYY-MM-DD hh:mm a').format('MM/DD/YYYY hh:mm a').toString();

    res.render('premGame', {
        homeScore : gameStats.home_score,
        homeTeam : gameStats.home_team,
        gameTime : gameStats.game_time,
        awayTeam : gameStats.away_team,
        awayScore : gameStats.away_score,
        gameId : gameStats.espn_id,
        homeId : gameStats.home_team_id,
        awayId : gameStats.away_team_id
    });
});







module.exports = router;