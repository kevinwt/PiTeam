var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');
var bcrypt = require('bcryptjs');
var userModel = require('./../public/javascripts/models/userModel');
var gamesModel = require('./../public/javascripts/models/gamesModel');
var playersModel = require('./../public/javascripts/models/playerStatsModel');
var ejs = require('ejs');
var schedule = require('node-schedule');
var pythonScrtipts = require(__dirname + '/../public/javascripts/runPythonScripts');
var moment = require('moment');

// Runs every day at 6:00 pm
let dailyGameScoreUpdate = schedule.scheduleJob('* 17 * * *', async function(){
// let dailyGameScoreUpdate = schedule.scheduleJob('*/1 * * * *', async function(){
    let response = [], todaysGames = [];

    console.log(moment().format('MM/DD/YYYY hh:mm a').toString() +  ': Run Score Update!');

    async function dbInteraction(game){
        if(game){
            let savedGame = await gamesModel.fetchGameEspn(game['Game ID']);
            todaysGames.push(game);
            if(savedGame){
                // Update already saved game information
                if((savedGame['home_score'] != game['Home Score'] || savedGame['away_score'] != game['Away Score']) || savedGame['game_time'] != game['Game Time'])
                    await gamesModel.updateGame(game);
            }else{
                // Save game information
                await gamesModel.saveGame(game);
            }

            return dbInteraction(response.shift());
        }else{
            dailyGamePlayerStatUpdate(todaysGames);
        }
    }
    let today = moment().format('YYYY/MM/DD').toString();
    // let today = '2018/11/10';

    try{
        response = await pythonScrtipts.scheduleScrape(today, today);
        dbInteraction(response.shift());
    }catch(e){
        console.log(e);
    }
});

// Runs with dailyGameScoreUpdate
let dailyGamePlayerStatUpdate = function(todaysGames){
    
    async function playerLoop(player, playerStats){
        if(player){
            let savedPlayer = await playersModel.fetchPlayerStatsSingleGame(player['game_id'], player['id']);
            if(savedPlayer){
                if(player['sub_time'])
                    player['sub_time'] = parseInt(player['sub_time']);
                await playersModel.updatePlayerSingleGameStats(player);
            }else{
                await playersModel.savePlayerSingleGameStats(player);
            }

            playerLoop(playerStats.shift(), playerStats);
        }else{
            return gamesLoop(todaysGames.shift());
        }
    }
    
    async function gamesLoop(game){
        if(game){
            let playerStats = await pythonScrtipts.statScrapeById(game['Game ID']);
            if(playerStats.length === 36){
                playerStats.forEach((player, i) => {
                    if(i < 18)
                        player['team_espn_id'] = game['Home ID'];
                    else
                        player['team_espn_id'] = game['Away ID'];
                });
                playerLoop(playerStats.shift(), playerStats);
            }else{
                console.error('ERROR: NOT ENOUGH PLAYERS FOUND DURRING SCRAPE!!!!');
                return 'Scrape Failed!';
            }
        }else{
            console.log('Completed Stats Import To DB!');
            return 'Done';
        }
    }

    if(todaysGames.length)
        gamesLoop(todaysGames.shift());
}

// Run every Monday at 6:00 pm
// Gets every game from Tuesday for the next 3 weeks so the future schedule stays up to date
let getFutureSchedule = schedule.scheduleJob('* 17 * * 1', async function(){
// let getFutureSchedule = schedule.scheduleJob('*/1 * * * *', async function(){    
    let counter = 0, today;
    let week = moment().format('YYYY/MM/DD');

    console.log('Run Future Schedule!');

    async function saveGameToDB(game){
        if(game){
            let savedGame = await gamesModel.fetchGameEspn(game['Game ID']);
            if(savedGame){
                if((savedGame['home_score'] != game['Home Score'] || savedGame['away_score'] != game['Away Score']) || savedGame['game_time'] != game['Game Time'])
                    await gamesModel.updateGame(game);
            }else{
                await gamesModel.saveGame(game);
            }
            return saveGameToDB(response.shift());
        }else{
            return 'Done';
        }
    }

    try{
        while(counter < 3){
            today = moment(week, 'YYYY/MM/DD').add(1, 'days').format('YYYY/MM/DD');
            week = moment(today, 'YYYY/MM/DD').add(6, 'days').format('YYYY/MM/DD');
            response = await pythonScrtipts.scheduleScrape(today, week);
            saveGameToDB(response.shift());
            counter++;
        }
    }catch(e){
        console.log(e);
    }
});

// var access = ['admin', 'owner'];

router.get('/',function(req, res, next){
    res.redirect('/admin/dashboard');
});

router.get('/dashboard', function(req, res, next){
    ejs.renderFile(__dirname + '/../views/admin_dashboard.ejs', {}, {}, function(err, tmplFile){
        if(err)
            console.error(err);

        baseTemplate(req, res, next, '<script type="text/javascript" src="/viewjs/adminDash.js"></script>', tmplFile);
    });
});

router.get('/dashboard/api/all_users', async function(req, res, next){
    await userModel.allUsers()
        .then((users)=>{
            res.send(users);
        });
});

router.post('/dashboard/view/user/:type', async function(req, res, next){
    let access = await userModel.allAccessKeys();
    if(req.params.type === 'new'){
        res.render('user_info',{
            user: {
                id: 0,
                username:'',
                first_name:'',
                last_name:'',
                email:'',
                access:''
            },
            access:access
        });
    }else{
        let user = await userModel.getUser(req.body.id);
        res.render('user_info',{
            user:user,
            access:access
        });
    }
});

router.post('/dashboard/api/save_user', async function(req, res, next){
    if(parseInt(req.body.id) === 0){
        bcrypt.hash(req.body.password, 12, async function(err, hash){
            if(err){
                console.message(err.message);
            }
            req.body.hash = hash;
            let result = await userModel.newUser(req.body);
            if(result.username === req.body.username){
                res.send({
                    text: result.username + ' added as a user.',
                    color: 'green'
                });
            }else{
                res.send({
                    text: 'Failed to add user ' + req.body.username,
                    color: 'red'
                });
            }
        });
    }else{
        bcrypt.hash(req.body.password, 12, async function(err, hash){
            req.body.hash = hash;
            console.log('req.body');
            console.log(req.body);
            let result = await userModel.updateUser(req.body);
            if(result.username === req.body.username){
                res.send({
                    text: 'Successfully updated ' + result.username,
                    color: 'green'
                });
            }else{
                res.send({
                    text: 'Failed to add user ' + req.body.username,
                    color: 'red'
                });
            }
        });
    }
});

router.post('/dashboard/api/delete_user', async function(req, res, next){
    let result = await userModel.deleteUser(req.body.id);
    res.send({
        text:'User deleted!',
        color:'green'
    });
});

module.exports = router;