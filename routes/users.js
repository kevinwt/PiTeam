var express = require('express');
var router = express.Router();
// var navbarjs = require('../public/javascripts/navbar');
var bodyParser = require('body-parser');
var bcrypt = require('bcryptjs');
var userModel = require('./../public/javascripts/models/userModel');
var ejs = require('ejs');


var urlencodedParser = bodyParser.urlencoded({extended : false});

router.get('/',function(req, res, next){
  res.redirect('/users/dashboard');
});

router.get('/dashboard', async function(req, res, next){

  if(!(req.session.user || {}).user_id)
    req.session.user = {
      user_id: 1
    };

  console.log(req.session.user);

  // Get all users leagues and teams
  let userTeams = await userModel.userDashTeamsLeagues((req.session.user || {}).user_id);

  console.log('userTeams');
  console.log(userTeams);

  if(userTeams.Error){
    console.error('ERROR GETTING LEAGUE/TEAM DATA!!!');
    console.error('Error Message: ' + userTeams.Error);
    console.error('Error Code: ' + userTeams.code);
  }

  let formatData = {};
  let leagueArr = [];

  console.log('GETS HERE');
  if(userTeams.leagues.length){
    userTeams.leagues.forEach(league => {
      //Add league to formatData
      formatData[league.id] = league;
      formatData[league.id].trans = [];
    });
  }

  if(userTeams.teams.length){
    // Add team to league in formatData
    userTeams.teams.forEach(team => {
      formatData[team.league_id].team = team;
    });
  }

  if(userTeams.capts.length){
    userTeams.capts.forEach(capt => {
      formatData[capt.league_id].captain = capt.player_id;
    });
  }

  if(userTeams.trans.length){
    userTeams.trans.forEach(tran => {
      try{
        formatData[tran.league_id].trans.push({
          player_out: tran.player_out,
          price_out: tran.price_out,
          player_in: tran.player_in,
          price_in: tran.price_in,
          date: tran.date
        });
      }catch(e){
        console.error('user.js (line: 71): Failed to add transfer to format data league!');
      }
    });
  }

  for(attr in formatData){
    leagueArr.push(formatData[attr]);
  }

  let week = await userModel.getWeek();

  console.log(leagueArr);

  ejs.renderFile(__dirname + '/../views/user_dashboard.ejs', {data: leagueArr, week_num:week.week_num}, {}, function(err, tmplFile){
    if(err)
      console.error(err);
    
    baseTemplate(req, res, next, '<script type="text/javascript" src="/viewjs/userDash.js"></script>', tmplFile);
  });
});

router.get('/new_league_modal', function(req, res, next){
  res.render('newLeagueModal', {});
});

router.get('/transfer/get_modal', async function(req, res, next){
  console.log(req.query);

  let transData = await userModel.getUserTransfer(req.query.trans_id, (req.session.user || {}).user_id);
  let player_list = await userModel.getGeneralPlayerTable();

  // console.log(player_list);
  transData = {
    trans: {
      player_in: false,
      player_out: false
    },
    playerList:player_list
  };

  ejs.renderFile(__dirname + '/../views/transferModal.ejs', transData, {}, function(err, tmplFile){
    if(err)
      console.error(err);
    
    res.send({
      data: transData,
      tmpl: tmplFile
    }); 
  });
});

router.post('/fantasy_team/transfer', async function(req, res, next){
  console.log(req.body);

  let scheduledTransfer = await userModel.setTransfer(req.body);

  res.send(scheduledTransfer);
});


router.get('/fantasy_team/view_team/:team_id', async function(req, res, next){

  let week = await userModel.getWeek();
  let team = await userModel.getFantasyTeam({team_id:req.params.team_id, user_id: (req.session.user || {}).user_id});
  let players = await userModel.getFantasyTeamPlayers({team_id:req.params.team_id, user_id: (req.session.user || {}).user_id});
  let transfers = await userModel.getUserTransfer({team_id: req.params.team_id, week: (week || {}).week_num});

  console.log(week);
  console.log({team_id:req.params.team_id, user_id: ((req.session.user || {}).user_id) || 1});
  console.log({team_id: req.params.team_id, week:(week || {}).week_num});

  let test_team = {
    team_id: req.params.team_id,
    players: [
      {
        'name': 'De Gea',
        'team': 'MUN',
        'points': 7,
        'position': 'GK',
        'cost': 8.5
      },
      {
        'name': 'David Louis',
        'team': 'CHE',
        'points': 1,
        'position': 'DEF',
        'cost': 7.1
      },
      {
        'name': 'Arnold',
        'team': 'LIV',
        'points': 7,
        'position': 'DEF',
        'cost': 7.3
      },
      {
        'name': 'Jones',
        'team': 'MUN',
        'points': -5,
        'position': 'DEF',
        'cost': 4.0
      },
      {
        'name': 'Alli',
        'team': 'TOT',
        'points': 2,
        'position': 'MID',
        'cost': 6.8
      },
      {
        'name': 'Hazard',
        'team': 'CHE',
        'points': 10,
        'position': 'MID',
        'cost': 9.2
      },
      {
        'name': 'De Brune',
        'team': 'MNC',
        'points': 5,
        'position': 'MID',
        'cost': 8.6
      },
      {
        'name': 'Kante',
        'team': 'CHE',
        'points': 2,
        'position': 'MID',
        'cost': 5.7
      },
      {
        'name': 'M. Salah',
        'team': 'LIV',
        'points': 7,
        'position': 'MID',
        'cost': 8.8
      },
      {
        'name': 'Lacazette',
        'team': 'ARS',
        'points': 14,
        'position': 'FOR',
        'cost': 8.2
      },
      {
        'name': 'Aubamaung',
        'team': 'ARS',
        'points': 2,
        'position': 'FOR',
        'cost': 8.8
      },
    ],
    transfers:[
      {
        'trans_id': 1,
        'player_out':{
          'name': 'Kante',
          'team': 'CHE',
          'points': 0,
          'position': 'MID',
          'cost':5.7
        },
        'player_in':{
          'name': 'Delph',
          'team': 'MNC',
          'points': 0,
          'position': 'MID',
          'cost':6.1
        },
      }
    ]
  };

  ejs.renderFile(__dirname + '/../views/fantasyTeam.ejs', test_team, {}, function(err, tmplFile){
    if(err)
      console.error(err);

    baseTemplate(req, res, next, '<script type="text/javascript" src="/viewjs/fantasyTeam.js"></script>', tmplFile);
  });
});

router.get('/view/fantasy_team/select_team', async function(req, res, next){
  let allPlayers = await userModel.getGeneralPlayerTable();

  ejs.renderFile(__dirname + '/../views/selectTeamModal.ejs', {allPlayers:allPlayers}, {}, function(err, tmplFile){
    res.send(tmplFile);
  });
});

router.get('/api/fantasy_team/prem_players', async function(req, res, next){
  res.send(await userModel.getGeneralPlayerTable());
});

router.get('/api/fantasy_team/user_team', async function(req, res, next){
  res.send([]);
});

router.post('/api/fantasy_team/user_team/add_player', async function(req, res, next){
  res.send({player_id:req.body.player, name:'TEST PLAYER', team_abv:'TST', position:'GK', cost:'0'});
});



// LOGIN FUNCTIONALITY
router.get('/login', function(req, res, next){
  res.render('login',{
      view: '<script type="text/javascript" src="/viewjs/login.js"></script>'
  });
});

router.post('/login', urlencodedParser, async function(req, res, next) {
  let username = req.body.username;
  let password = req.body.password;
  let users = await userModel.getUserLoginByUsername(username.toString());
  req.session.user = {};

  if(users.length === 0){
    res.send({status:'Failed', message:'Incorrect username or password!'});
  }else{
    function comparePass(profile){
      if(profile){
        bcrypt.compare(password, profile.password, function(err, match){

          if(err)
            console.error(err);
          if(match){
            req.session.user = {
              user_id: profile.id,
              username:profile.username,
              name: profile.first_name + ' ' + profile.last_name,
              access: profile.key_name
            };
            res.send({status:'Success', message:'Logging in . . .'});
          }else{
            comparePass(users.shift());
          }
        });
      }else{
        res.send({status:'Failed', message:'Incorrect username or password!'});
      }
    }
    comparePass(users.shift());
  }
});

router.post('/league/:action', async function(req, res, next){
  console.log('League Check!');
  // console.log(await userModel.checkLeagueTaken(req.body.league_name, req.body.password));

  let check = await userModel.checkLeagueTaken(req.body.league_name, req.body.password);
  console.log(check);
  if(req.params.action === 'create'){
    if(!check){
      let newLeague = await userModel.createNewLeague(req.body.league_name, 1, req.body.password);
      res.send({
        status: 'Success',
        message: 'League Created Successfully!',
        color: 'green',
        leagueInfo:newLeague
      });
    }else{
      res.send({
        status: 'Fail',
        message: 'League Creation Failed! (League Name And/Or Password Invalid)!',
        color: 'red'
      });
    }
  }else if(req.params.action === 'join'){
    if(!check){
      res.send({
        status: 'Fail',
        message: 'Failed To Join League! (No League Found With Those Credentials!)',
        color: 'red'
      });
    }else{
      res.send({
        status: 'Success',
        message: 'Request To Join League Successful!',
        color: 'green'
      });
    }
  }else{
    next();
  }
});

module.exports = router;
