var express = require('express');
var router = express.Router();
// var navbarjs = require('../public/javascripts/navbar');
var bodyParser = require('body-parser');
var gameStatsModel = require('./../public/javascripts/models/gameStatsModel');
var playerStatsModel = require('./../public/javascripts/models/playerStatsModel');
var moment = require('moment');
var ejs = require('ejs');

let baseTemplate = function(req, res, next, viewFile){
    res.render('defaultTemplate', {
        view:viewFile
    });
}

router.get('/', function(req, res, next){
    baseTemplate(req, res, next, '<script type="text/javascript" src="/viewjs/playerDash.js"></script>')
});

router.get('/:location/tripple_wedge/:size', async function(req, res, next){
    if(req.params.location === 'dashboard'){
        let playerStreaks = {
            goals:[],
            assists:[],
            saves:[],
            cleanSheets:[],
        };
        let ids = await playerStatsModel.FetchAllEspnId();

        // console.log(ids);
        function pushStreak(attr, stats){
            let index = 0, insert = false;
            if(playerStreaks[attr].length === 10 && playerStreaks[attr][9][attr] >= stats[attr]){
                    return false;
            }else{
                if(playerStreaks[attr].length === 0){
                    playerStreaks[attr].push(stats);
                }else{
                    while(index < playerStreaks[attr].length){
                        if(playerStreaks[attr][index][attr] < stats[attr]){
                            playerStreaks[attr].splice((index-1), 0, stats);
                            playerStreaks[attr].pop();
                            return true;
                        }
                        index++;
                    }
                    if(index < 10 && insert === false){
                        playerStreaks[attr].push(stats);
                    }
                }
            }
        }

        function groupStats(players){
            players.forEach(async (row, i)=>{
                let test = -1;
                await playerStatsModel.fetchLastFourGames(row.espn_id, moment().format('YYYY-MM-DD').toString())
                    .then((lastFour)=>{
                        if(lastFour){
                            if(lastFour.goals && lastFour.goals !== 0)
                                pushStreak('goals', lastFour);
                            if(lastFour.assists && lastFour.assists !== 0)
                                pushStreak('assists', lastFour);
                            if(lastFour.saves && lastFour.saves !== 0)
                                pushStreak('saves', lastFour);
                        }
                    });
                if(i === (players.length - 1))
                    return true;
            });
        }

        groupStats(ids);

        // res.send(playerStreaks);

        res.render('players_dashboard', {
            playerStreaks: playerStreaks,
            wedge1: {
                image:"/images/pi-stand-in-image.png",
                alt:"Dummy Image",
                text:"Dummy Text",
                size: req.params.size
            },
            wedge2: {
                image:"/images/pi-stand-in-image.png",
                alt:"Dummy Image",
                text:"Dummy Text",
                size: req.params.size
            },
            wedge3: {
                image:"/images/pi-stand-in-image.png",
                alt:"Dummy Image",
                text:"Dummy Text",
                size: req.params.size
            }
        });
    }
});


router.get('/single_game_stats/:type/:gameId', async function(req, res, next){
    let stats = await playerStatsModel.fetchSingleGamePlayerStats(req.params.gameId);
    let gameInfo = await gameStatsModel.fetchGameTime(req.params.gameId);
    let homeTeamPlayers = [], awayTeamPlayers = [];
    gameInfo.game_time = moment(gameInfo.game_time, 'YYYY-MM-DD hh:mm a').format('YYYY-MM-DD').toString();

    let getTransfers = function(arr){
        arr.forEach(async (player, index)=>{
            let teams = await playerStatsModel.fetchAllPlayerTransfers(player.player_id);
            for(let i = 0; i < teams.length; i++){
                if(moment(teams[i].date_changed, 'YYYY-MM-DD').isSameOrBefore(gameInfo.game_time)){
                    player.active_team = {team_id: teams[i].new_team_id, team_name:teams[i].team_name};
                    // console.log(player.active_team);
                    if(player.active_team.team_id.toString() === req.query.homeId){
                        homeTeamPlayers.push(player);
                    }else if(player.active_team.team_id.toString() === req.query.awayId){
                        awayTeamPlayers.push(player);
                    }else{
                        console.log(req.query.homeId + ' !== ' + player.active_team.team_id + ' !== ' + req.query.awayId);
                    }
                    break;
                }
            }

            if(index === (arr.length - 1)){
                if(req.params.type === 'view'){
                    res.render('singleGamePlayerStats', {
                        homePlayers : homeTeamPlayers,
                        awayPlayers : awayTeamPlayers
                    });
                }else if(req.params.type === 'api'){
                    res.send({
                        homeTeam: {
                            teamName: homeTeamPlayers[0].active_team.team_name,
                            players : homeTeamPlayers
                        },
                        awayTeam: {
                            teamName: awayTeamPlayers[0].active_team.team_name,
                            players : awayTeamPlayers
                        }
                    });
                }
            }
        });
    }

    await getTransfers(stats);
});







module.exports = router;