var express = require('express');
var router = express.Router();
const sqlite3 = require('sqlite3').verbose();
var statsCalc = require('./../public/javascripts/statsCalc');
// var navbarjs = require('../public/javascripts/navbar');
var path = require('path');
var async = require('async');
var bodyParser = require('body-parser');
var pythonShell = require('python-shell');
var Promise = require('bluebird');
var gamesModel = require('./../public/javascripts/models/gamesModel');
var updatesModel = require('./../public/javascripts/models/updatesModel');
var moment = require('moment');

// pythonShell.defaultOptions = {scriptPath: '/public/pythonscripts'};

var urlencodedParser = bodyParser.urlencoded({extended : false});

//Goals SQL: SELECT COUNT (*), PL.PID FROM PLGoals1718 GL, players PL WHERE PL.PID = GL.Scorer AND GL.OG != 1 GROUP BY PL.PID
//Assists SQL: SELECT COUNT (*), PL.PID FROM PLGoals1718 GL, players PL WHERE PL.PID = GL.Assister GROUP BY PL.PID

//Most recent 4 Games Goals SQL: SELECT COUNT (*), PL.PID, FROM PLGoals1718 GL, players PL WHERE (GL.Scorer = PL.PID AND GL.OG != 1) 
//      AND GL.ID IN (SELECT GM.GID FROM PLGames1718 GM WHERE GM.GameWeek > ((SELECT MIN(GM1.GameWeek) FROM PLGames1718 GM1
//      WHERE Status = 'UP') -5) AND GM.GameWeek < (SELECT MIN(GM2.GameWeek) FROM PLGames1718 GM2 WHERE Status = 'UP')) 
//      GROUP BY PL.PID;

function tableHeader(pos){

    var table = '<table class="table-hover table-striped table-bordered table-responsive">';
    table += '<tr><th colspan=17><b>' + pos + '</b></th></tr>';
    table += "<tr>";
    table += "<th>Position</th>";
    table += "<th>Last Name</th>";
    table += "<th>First Name</th>";
    table += "<th>Team</th>";
    table += "<th>Cost</th>";
    table += "<th>Points</th>";
    table += "<th>Starts</th>";
    table += "<th>Sub-Ins</th>";
    table += "<th>Goals</th>";
    table += "<th>Assists</th>";
    table += "<th>Clean Sheets</th>";
    table += "<th>Goals Conceded</th>";
    table += "<th>Red Cards</th>";
    table += "<th>Saves</th>";
    table += "<th>Pens Missed</th>";
    table += "<th>PPG</th>";
    table += "<th>PPM</th>";
    table += "</tr>";

    console.log('built table header');
    return table;
}

//Builds table format for games page
function gamesHeader(GW){
    var table = '<table class="table-hover table-striped table-bordered table-responsive">';
    table += '<tr><th colspan=4><b>Gameweek ' + GW + '</b></th></tr>';
    table += "<tr>";
    table += "<th>Home Score</th>";
    table += "<th>Home Team</th>";
    table += "<th>Away Team</th>";
    table += "<th>Away Score</th>";
    table += "</tr>";

    return table;
}


//Builds an html formated table row by row using appropiate JSON
function buildPlayerTable(pos, players){
    var posJSON;
    var table = '';

    if(pos.match(/GK|DEF|MID|FOR/)){
        players.forEach(function(entry){
            if(entry.Pos == pos){
                table += '<tr>';
                table += '<td>' + entry.Pos + "</td>";
                table += '<td>' + entry.LastName + "</td>";
                table += '<td>' + entry.FirstName + "</td>";
                table += '<td>' + entry.TeamAbv + "</td>";
                table += '<td>' + entry.Cost + " m</td>";
                table += '<td>' + entry.Points + "</td>";
                table += '<td>' + entry.Starts + "</td>";
                table += '<td>' + entry.SubIns + "</td>";
                table += '<td>' + entry.Goals + "</td>";
                table += '<td>' + entry.Assists + "</td>";
                table += '<td>' + entry.CleanSheets + "</td>";
                table += '<td>' + entry.GoalsConceded + "</td>";
                table += '<td>' + entry.RedCards + "</td>";
                table += '<td>' + entry.Saves + "</td>";
                table += '<td>' + entry.PensMissed + "</td>";
                table += '<td>' + entry.PPG + "</td>";
                table += '<td>' + entry.PPM + "</td>";
                table += '</tr>';
            }
        });
    }else if(pos == 'OP'){
        players.forEach(function(entry){
            if(entry.Pos != 'GK'){
                table += '<tr>';
                table += '<td>' + entry.Pos + "</td>";
                table += '<td>' + entry.LastName + "</td>";
                table += '<td>' + entry.FirstName + "</td>";
                table += '<td>' + entry.TeamAbv + "</td>";
                table += '<td>' + entry.Cost + " m</td>";
                table += '<td>' + entry.Points + "</td>";
                table += '<td>' + entry.Starts + "</td>";
                table += '<td>' + entry.SubIns + "</td>";
                table += '<td>' + entry.Goals + "</td>";
                table += '<td>' + entry.Assists + "</td>";
                table += '<td>' + entry.CleanSheets + "</td>";
                table += '<td>' + entry.GoalsConceded + "</td>";
                table += '<td>' + entry.RedCards + "</td>";
                table += '<td>' + entry.Saves + "</td>";
                table += '<td>' + entry.PensMissed + "</td>";
                table += '<td>' + entry.PPG + "</td>";
                table += '<td>' + entry.PPM + "</td>";
                table += '</tr>';
            }
        });
    }else{
        players.forEach(function(entry){
            table += '<tr>';
            table += '<td>' + entry.Pos + "</td>";
            table += '<td>' + entry.LastName + "</td>";
            table += '<td>' + entry.FirstName + "</td>";
            table += '<td>' + entry.TeamAbv + "</td>";
            table += '<td>' + entry.Cost + " m</td>";
            table += '<td>' + entry.Points + "</td>";
            table += '<td>' + entry.Starts + "</td>";
            table += '<td>' + entry.SubIns + "</td>";
            table += '<td>' + entry.Goals + "</td>";
            table += '<td>' + entry.Assists + "</td>";
            table += '<td>' + entry.CleanSheets + "</td>";
            table += '<td>' + entry.GoalsConceded + "</td>";
            table += '<td>' + entry.RedCards + "</td>";
            table += '<td>' + entry.Saves + "</td>";
            table += '<td>' + entry.PensMissed + "</td>";
            table += '<td>' + entry.PPG + "</td>";
            table += '<td>' + entry.PPM + "</td>";
            table += '</tr>';
        });
    }
    //console.log('table: ' + table);
    return table;
}


/* GET home page. */
router.get('/', function(req, res, next) {
    res.redirect('/users/login');
});

/* router.get('/general/login', function(req, res, next){
    res.render('login',{
        view: '<script type="text/javascript" src="/viewjs/playerDash.js"></script>'
    });
}) */


router.get('/:id', function(req, res, next){
    if(req.params.id == 'Games'){
        var query1 = "CREATE VIEW IF NOT EXISTS games(GameWeek, HomeScore, HomeTeam, AwayTeam, AwayScore) AS SELECT GM.GameWeek, GM.HomeScore, TM.TeamName, TM2.TeamName, GM.AwayScore FROM PLteams1718 TM, PLteams1718 TM2, PLGames1718 GM WHERE GM.HomeTeam = TM.TID AND GM.AwayTeam = TM2.TID ORDER BY GM.GameWeek, GM.GID";
        var query2 = "SELECT * FROM games";
        var navbar = navbarjs.buildNav(2);
        var html = '';
        var GW = 0;

        let db = new sqlite3.Database('./databases/players.db', (err) => {
            if (err) {
                console.error(err.message);
            }else{
                console.log('Connected to the players database.');
            }
        });
        
        //Execute sql statements in synchronous fashion
        db.serialize(() => {

            //db.run("DROP VIEW games");
            db.run(query1);
    
            db.each(query2, function(err,row) {
                if (err) {
                    console.error(err.message);
                }
    
                if(GW < row.GameWeek){
                    GW = row.GameWeek;
                    if(GW != 1){
                        html += '</table><br>\n';
                    }
                    html += gamesHeader(GW);
                }

                //Format each Games stat into html table form
                html += "<tr>";
                //html += '<td>' + row.GameWeek + '</td>';
                if(row.HomeScore == null){
                    html += '<td>-</td>';
                    html += '<td>' + row.HomeTeam + '</td>';
                }else if(row.HomeScore > row.AwayScore){
                    html += '<td><b>' + row.HomeScore + '</b></td>';
                    html += '<td><b>' + row.HomeTeam + '</b></td>';
                }else{
                    html += '<td>' + row.HomeScore + '</td>';
                    html += '<td>' + row.HomeTeam + '</td>';
                }

                if(row.AwayScore == null){
                    html += '<td>' + row.AwayTeam + '</td>';
                    html += '<td>-</td>';
                }else if(row.AwayScore > row.HomeScore){
                    html += '<td><b>' + row.AwayTeam + '</b></td>';
                    html += '<td><b>' + row.AwayScore + '</b></td>';
                }else{
                    html += '<td>' + row.AwayTeam + '</td>';
                    html += '<td>' + row.AwayScore + '</td>';
                }
                    html += '</tr>';
        
            }, function(){
                html += "</table>";

                //Closes players database
                db.close((err) => {
                    if (err) {
                        console.error(err.message);
                    }
                    console.log('Close the database connection.');
                });
                console.log('Rendered to page');

                res.render('index', { 
                    nav : navbar,
                    print: html 
                });
            });
        });
    //Compares id of URL against regex below since they use the same page template
    }else if(req.params.id.match(/Players|FOR|MID|DEF|GK/)){

        var activeVal = {'Players':1, 'FOR':6, 'MID':5, 'DEF':4, 'GK':3};
        //Builds navbar
        var navbar = navbarjs.buildNav(activeVal[req.params.id]);
        //Builds table header
        var html = tableHeader(req.params.id);

        var players = [];

        //Checks if this is the first time one of these pages has been loaded
        

        let db = new sqlite3.Database('./databases/players.db', (err) => {
            if (err) {
                console.error(err.message);
            }else{
                console.log('Connected to the players database.');
            }
        });
    
        db.each("SELECT *, t.team_name, T.team_abv FROM prem_league_players P, prem_league_teams T WHERE P.team_id = T.team_id AND P.team_id != 500 ORDER BY T.team_abv", function(err,row) {
            if (err) {
                console.error(err.message);
            }

            // console.log(row);
            var myObject = {};

            row.cost === null || row.cost === 'null' ? row.cost = 'N/A' : 
                row.cost === '' || row.cost === undefined ? row.cost = 'N/A' :
                    row.cost = row.cost.toFixed(1);
    
            //Calculate Points per Game and Points per Million for each player
            /* var PPG = statsCalc.PPUnit(row.Points, (row.Starts + row.SubIns)).toFixed(2);
            var PPM = statsCalc.PPUnit(row.Points, row.Cost).toFixed(2); */
    
            //Format each players stat into a JSON object
            myObject.PID = row.player_id;
            myObject.Pos = row.position;
            myObject.LastName = row.last_name;
            myObject.FirstName = row.first_name;
            myObject.TeamAbv = row.team_abv;
            myObject.Cost = row.cost;
            myObject.Points = 0;
            myObject.Starts = row.starts;
            myObject.SubIns = row.subs;
            myObject.Goals = row.goals;
            myObject.Assists = row.assists;
            myObject.CleanSheets = row.clean_sheets;
            myObject.GoalsConceded = row.goals_conceeded;
            myObject.RedCards = row.red_cards;
            myObject.Saves = row.saves;
            myObject.PensMissed = row.pens_missed;
            myObject.PPG = 0;
            myObject.PPM = 0;

            players.push(myObject);

        }, function () {
            html += buildPlayerTable(req.params.id, players);
            html += '</table>';

            db.close((err) => {
                if (err) {
                    console.error(err.message);
                }
                    console.log('Close the database connection.');
            });

            res.render('index', { 
                nav : navbar,
                print: html 
            });
        });
    }else if(req.params.id.match(/ARS|BHA|BOU|BUR|CHE|CRY|EVE|HUD|LEI|LIV|MNC|MUN|NEW|SOT|STK|SWA|TOT|WAT|WBA|WHU/)){
        var navbar = navbarjs.buildNav(7);        
        var logo = '<img src="/images/' + req.params.id + '.png" alt="' + req.params.id + ' logo" width=350px height=350px>';
        var GKtable = tableHeader('Goalkeepers');
        var PLtable = tableHeader('Outfield Players');
        var test = '';
        var players = [];

        let db = new sqlite3.Database('./databases/players.db', (err) => {
            if (err) {
                console.error(err.message);
            }else{
                console.log('Connected to the players database.');
            }
        });
    
        db.each("SELECT * FROM players WHERE TeamAbv = ? ORDER BY Points DESC", [req.params.id], function(err,row) {
            if (err) {
                console.error(err.message);
            }
            var myObject = new Object();
    
            //Calculate Points per Game and Points per Million for each player
            var PPG = statsCalc.PPUnit(row.Points, (row.Starts + row.SubIns)).toFixed(2);
            var PPM = statsCalc.PPUnit(row.Points, row.Cost).toFixed(2);
    
            //Format each players stat into a JSON object
            myObject.PID = row.PID;
            myObject.Pos = row.Pos;
            myObject.LastName = row.LastName;
            myObject.FirstName = row.FirstName;
            myObject.TeamAbv = row.TeamAbv;
            myObject.Cost = row.Cost;
            myObject.Points = row.Points;
            myObject.Starts = row.Starts;
            myObject.SubIns = row.SubIns;
            myObject.Goals = row.Goals;
            myObject.Assists = row.Assists;
            myObject.CleanSheets = row.CleanSheets;
            myObject.GoalsConceded = row.GoalsConceded;
            myObject.RedCards = row.RedCards;
            myObject.Saves = row.Saves;
            myObject.PensMissed = row.PensMissed;
            myObject.PPG = PPG;
            myObject.PPM = PPM;

            players.push(myObject);

        }, function () {
            GKtable += buildPlayerTable('GK', players);
            PLtable += buildPlayerTable('OP', players);
            GKtable += '</table>';
            PLtable += '</table>';

            db.close((err) => {
                if (err) {
                   console.error(err.message);
                }
                   console.log('Close the database connection.');
            });

            res.render('team', { 
                nav : navbar,
                TeamImage: logo,
                TeamStats: test,
                GKstats: GKtable,
                PLstats: PLtable
            });
        });
    }else if(req.params.id == 'pick'){
        var navbar = navbarjs.buildNav(8);

        var GKs = '';
        var DEFs = '';
        var MIDs = '';
        var FORs = '';
        var players = [];
        
        let db = new sqlite3.Database('./databases/players.db', (err) => {
            if (err) {
                console.error(err.message);
            }else{
                console.log('Connected to the players database.');
            }
        });
            
        db.each("SELECT * FROM players ORDER BY LastName", function(err,row) {
            if (err) {
                console.error(err.message);
            }
            var myObject = new Object();
            
            //Calculate Points per Game and Points per Million for each player
            var PPG = statsCalc.PPUnit(row.Points, (row.Starts + row.SubIns)).toFixed(2);
            var PPM = statsCalc.PPUnit(row.Points, row.Cost).toFixed(2);
        
            //Format each players stat into a JSON object
            myObject.PID = row.PID;
            myObject.Pos = row.Pos;
            myObject.LastName = row.LastName;
            myObject.FirstName = row.FirstName;
            myObject.TeamAbv = row.TeamAbv;
            myObject.Cost = row.Cost;
            myObject.Points = row.Points;
            myObject.Starts = row.Starts;
            myObject.SubIns = row.SubIns;
            myObject.Goals = row.Goals;
            myObject.Assists = row.Assists;
            myObject.CleanSheets = row.CleanSheets;
            myObject.GoalsConceded = row.GoalsConceded;
            myObject.RedCards = row.RedCards;
            myObject.Saves = row.Saves;
            myObject.PensMissed = row.PensMissed;
            myObject.PPG = PPG;
            myObject.PPM = PPM;
        
            players.push(myObject);
        
        }, function () {
        
            db.close((err) => {
                if (err) {
                    console.error(err.message);
                }
                    console.log('Close the database connection.');
            });

            GKs += '<button class="sidenav-drpdwn sidenav-active" id="GKBtn">GK</button>';
            GKs += '<div id="GKsDisp" class="sidenav-content">';
            GKs += '<div class="sidenav-sub">';
            GKs += '<ul class="selectPL">';
            players.forEach(function(entry){
                if(entry.Pos == 'GK'){
                    GKs += '<li class="pick" id="' + entry.PID + '"><a id="' + entry.Pos + entry.PID + '" class="select ' + entry.TeamAbv + '">' + entry.LastName + '</a></li>';
                }
            });
            GKs += '</ul></div></div>';
            
            DEFs+= '<button class="sidenav-drpdwn" id="DEFBtn">DEF</button>';
            DEFs += '<div id="DEFsDisp" class="sidenav-content">';
            DEFs += '<div class="sidenav-sub">';
            DEFs += '<ul class="selectPL">';
            players.forEach(function(entry){
                if(entry.Pos == 'DEF'){
                    DEFs += '<li class="pick" id="' + entry.PID + '"><a id="' + entry.Pos + entry.PID + '" class="select ' + entry.TeamAbv + '">' + entry.LastName + '</a></li>';
                }
            });
            DEFs += '</ul></div></div>';
            
            MIDs += '<button class="sidenav-drpdwn" id="MIDBtn">MID</button>';
            MIDs += '<div id="MIDsDisp" class="sidenav-content">';
            MIDs += '<div class="sidenav-sub">';
            MIDs += '<ul class="selectPL">';
            players.forEach(function(entry){
                if(entry.Pos == 'MID'){
                    MIDs += '<li class="pick" id="' + entry.PID + '"><a id="' + entry.Pos + entry.PID + '" class="select ' + entry.TeamAbv + '">' + entry.LastName + '</a></li>';
                }
            });
            MIDs += '</ul></div></div>';
            
            FORs += '<button class="sidenav-drpdwn" id="FORBtn">FOR</button>';
            FORs += '<div id="FORsDisp" class="sidenav-content">';
            FORs += '<div class="sidenav-sub">';
            FORs += '<ul class="selectPL">';
            players.forEach(function(entry){
                if(entry.Pos == 'FOR'){
                    FORs += '<li class="pick" id="' + entry.PID + '"><a id="' + entry.Pos + entry.PID + '" class="select ' + entry.TeamAbv + '">' + entry.LastName + '</a></li>';
                }
            });
            FORs += '</ul></div></div>';

        
            res.render('pick', {
                nav : navbar,
                GKs : GKs,
                DEFs : DEFs,
                MIDs : MIDs,
                FORs : FORs
            });
        });

       
    }else{
        
    }
});

router.get('/admin/:actID', function(req, res, next) {
    if(req.params.actID == 'lineup'){

        var players = [];
        var navbar = navbarjs.buildNav(0);

        var html = '<form action="/admin/lineup" method="post" id="lineup">';
        html += 'Game ID:<br>';
        html += '<input type="text" name="GID" id="GID"><br>';
        html += '<select name="TID" id="TID">';
        html += '<option value="1">Arsenal</option>';
        html += '<option value="2">Brighton & Hove Albion</option>';
        html += '<option value ="3">AFC Bournemouth</option>';
        html += '<option value ="4">Burnley</option>';
        html += '<option value ="5">Chelsea</option>';
        html += '<option value ="6">Crystal Palace</option>';
        html += '<option value ="7">Everton</option>';
        html += '<option value ="8">Huddersfield</option>';
        html += '<option value ="9">Leicester</option>';
        html += '<option value ="10">Liverpool</option>';
        html += '<option value ="11">Manchester City</option>';
        html += '<option value ="12">Manchester United</option>';
        html += '<option value ="13">New Castle</option>';
        html += '<option value ="14">Southampton</option>';
        html += '<option value ="15">Stoke City</option>';
        html += '<option value ="16">Swansea City</option>';
        html += '<option value ="17">Tottenham</option>';
        html += '<option value ="18">Watford</option>';
        html += '<option value ="19">West Bromwich Albion</option>';
        html += '<option value ="20">West Ham United</option>';
        html += '</select><br>';

        let db = new sqlite3.Database('./databases/players.db', (err) => {
            if (err) {
                console.error(err.message);
            }else{
                console.log('Connected to the players database.');
            }
        });
    
        db.each("SELECT * FROM players ORDER BY TeamAbv", function(err,row) {
            if (err) {
                console.error(err.message);
            }
            var myObject = new Object();
    
            //Format each players stat into a JSON object
            myObject.PID = row.PID;
            myObject.LastName = row.LastName;
            myObject.FirstName = row.FirstName;

            players.push(myObject);

        }, function() {

            players.forEach(function(entry){
                html += '<input type="checkbox" name=player' + entry.PID +'" value=' + entry.PID + '>' + entry.LastName + ', ' + entry.FirstName + '<br>';
            });

            html += '<input type="submit" value="Submit" id="submit">';

            html += '</form>';



            res.render('startSubs', { 
                nav : navbar,
                print: html 
            });
        });
    }else if(req.params.actID === 'python_scripts'){

        var navbar = navbarjs.buildNav(0);
        var html = '<h3 class="center-cont"> Python Script </h3><br>';
        html += '<div class="center-cont"><label for="start_date"><span class="col-md-2">Start Date:</span><input type="text" id="start_date" class="form-control col-md-5 date-input"></label><label for="end_date"><span class="col-md-2">End Date:</span><input type="text" id="end_date" class="form-control col-md-5 date-input"></label></div>';
        html += '<div class="center-cont"><input type="button" value="Fetch Stats" class="btn btn-primary" id="games_btn"></div>';
        res.render('scrape', { 
            nav : navbar,
            print: html
        });

    }else if(req.params.actID === 'player_alterations'){
        var navbar = navbarjs.buildNav(0);
        var table = '';
        var view = '<script type="text/javascript" src="/javascripts/views/alter_player.js"></script>';        

        table += '<table id="cost_players" class="table-hover table-striped table-bordered table-responsive margin-bot-10">';
        table += '<tr>';
        table += '<th colspan="6" class="tbl-txt-center">All Players Table</th>';
        table += '</tr>';
        table += '<tr>';
        table += '<th class="tbl-txt-center">First Name</th>';
        table += '<th class="tbl-txt-center">Last Name</th>';
        table += '<th class="tbl-txt-center">Team</th>';
        table += '<th class="tbl-txt-center">Position</th>';
        table += '<th class="tbl-txt-center">Cost</th>';
        table += '<th class="tbl-txt-center">Injured</th>';
        table += '</tr>';

        updatesModel.allPlayers()
        .then(function(players){
            players.forEach(function(entry, i){
                if(entry.cost !== null && entry.cost !== 'null')
                    entry.cost = entry.cost.toFixed(1);

                if(i % 2 === 0)
                    table += '<tr class="player-row-even admin-player-row" data-id="' + entry.player_id + '" data-espn="' + entry.espn_id + '">';
                else
                    table += '<tr class="player-row-odd admin-player-row" data-id="' + entry.player_id + '" data-espn="' + entry.espn_id + '">';
                table += '<td class="player-first">' + entry.first_name + '</td>';
                table += '<td class="player-last">' + entry.last_name + '</td>';
                table += '<td class="player-team" data-team="' + entry.team_id + '">' + entry.team_name + '</td>';
                table += '<td class="player-position">' + entry.position + '</td>';
                table += '<td class="player-cost">' + entry.cost + '</td>';
                table += '<td class="player-injured">' + entry.injured + '</td>';
                table += '</tr>';
            });
        
            res.render('alter_player', { 
                nav : navbar,
                players_table : table,
                view: view
            });
        });
    }
});

router.get('/admin/api/add_player', function(req, res, next){
    updatesModel.addPlayer(req.query)
    .done(function(result){
        var player = {};
        player.raw = result;
        if(result.injured)
            result.injured = 1;
        else
            result.injured = 0;

        player.team_id = result.team_id;
        player.row += '<tr class="admin-player-player.row added" data-id="' + result.player_id + '" data-espn="' + result.espn_id + '">';
        player.row += '<td class="player-first">' + result.first + '</td>';
        player.row += '<td class="player-last">' + result.last + '</td>';
        player.row += '<td class="player-team" data-team="' + result.team_id + '">' + result.team + '</td>';
        player.row += '<td class="player-position">' + result.position + '</td>';
        player.row += '<td class="player-cost">' + result.cost + '</td>';
        player.row += '<td class="player-injured">' + result.injured + '</td>';
        player.row += '</tr>';
        res.send(player);
    });
});

router.post('/admin/api/:action', async function(req, res, next){
    if(req.params.action === 'upd_player'){
        updatesModel.playerUpdate(req.body)
        .then(function(result){
            var data = {raw:result};

            console.log(typeof result.cost);

            if(typeof result.cost !== "string")
                result.cost = result.cost.toFixed(1);

            data.row += '<td class="player-first">' + result.first + '</td>';
            data.row += '<td class="player-last">' + result.last + '</td>';
            data.row += '<td class="player-team" data-team="' + result.team_id + '">' + result.team + '</td>';
            data.row += '<td class="player-position">' + result.position + '</td>';
            data.row += '<td class="player-cost">' + result.cost + '</td>';
            data.row += '<td class="player-injured">' + result.injured + '</td>';
            res.send(data);
        });
    }
});

router.get('/admin/api/player_search', function(req, res, next){
    gamesModel.playerSearch(req.query)
    .then(function(rows){
        var html = '<table class="table-hover table-striped table-bordered table-responsive">';
        html += '<tr>';
        html += '<td colspan="7">Players Found</td>';
        html += '</tr>';
        html += '<tr>';
        html += '<th>Player ID</th>';
        html += '<th>ESPN ID</th>';
        html += '<th>First Name</th>';
        html += '<th>Last Name</th>';
        html += '<th>Team</th>';
        html += '<th>Position</th>';
        html += '<th>Cost</th>';
        html += '</tr>';

        if(rows.length <= 0){
            html += '<tr>';
            html += '<td colspan="7">No Matching Players Found</td>';
            html += '</tr>';
        }else{
            rows.forEach(function(entry){
                html += '<tr>';
                html += '<td>' + entry.player_id + '</td>';
                html += '<td>' + entry.espn_id + '</td>';
                html += '<td>' + entry.first_name + '</td>';
                html += '<td>' + entry.last_name + '</td>';
                html += '<td>' + entry.team_name + '</td>';
                html += '<td>' + entry.position + '</td>';
                html += '<td>' + entry.cost + '</td>';
                html += '</tr>';
            });
        }

        html += '</table>';

        res.send(html);
    });
});

router.get('/admin/api/teams_dropdown', function(req, res, next){
    console.log(req.query);
    updatesModel.teams_dropdown()
    .then(function(data){
        var team_dropdown = '<select id="' + req.query.idOpt + '" class="' + req.query.classOpt + '">';
        data.forEach(function(team){
            if(req.query.defaultOpt){
                /* console.log(team.team_id);
                console.log(req.query.defaultOpt === team.team_id.toString()); */
                if(req.query.defaultOpt === team.team_id.toString()){
                    team_dropdown += '<option value="' + team.team_id + '" selected="selected">' + team.team_name + '</option>';
                }else{
                    team_dropdown += '<option value="' + team.team_id + '">' + team.team_name + '</option>';
                }
            }else{
                team_dropdown += '<option value="' + team.team_id + '">' + team.team_name + '</option>';
            }
        });
        team_dropdown += '</select>';
        res.send(team_dropdown);
    });
});

router.get('/admin/api/scrape_games', function (req, res, next){
    let test = new Promise(function (resolve, reject){
        console.log('Fetching Stats . . .');

        console.log(req.query);
        pythonShell.run('/public/pythonscripts/scrapeMain.py', {args: [parseInt(req.query.start_date), parseInt(req.query.end_date)]}, function (err, results){
            if(err) 
                reject(err);
            else{
                resolve(results);
            }
        });
    }).then(async (Data) => {
        let game_arr = [];
        let player_arr = [];
        let retGames = [];
        let retPlayers = [];
        let current_game_id = 0;

        var createObj = function(data){
            playerCnt = 0;
            for(i = 0; i < data.length; i++){
                if(data[i].match(/Game Object/)){
                    var game_obj = {type:'Game'};
                    game_obj.espn_id = parseInt(data[i+1]);
                    game_obj.game_time = data[i+2];
                    game_obj.game_time = moment(data[i+2], 'YYYY-MM-DDTHH:mmZ').format('YYYY-MM-DD hh:mm a').toString();

                    console.log(game_obj.game_time);
                    game_obj.home_id = parseInt(data[i+3]);
                    game_obj.away_id = parseInt(data[i+4]);
                    if(data[i+5].match(/Score/)){
                        game_obj.home_score = parseInt(data[i+6]);
                        game_obj.away_score = parseInt(data[i+7]);
                    }
                    current_game_id = game_obj.espn_id;
                    game_arr.push(game_obj);
                    i+=7;
                }else if(data[i].match(/Player/) || data[i].match(/Goalie/)){
                    var player_obj = {type:'Player'};
                    player_obj.id = parseInt(data[i+1]);
                    player_obj.game_id = current_game_id || 0;
                    player_obj.goals = parseInt(data[i+2]);
                    player_obj.saves = parseInt(data[i+3]);
                    player_obj.assists = parseInt(data[i+4]);
                    player_obj.shots = parseInt(data[i+5]);
                    player_obj.shots_on_target = parseInt(data[i+6]);
                    player_obj.clean_sheet = parseInt(data[i+7]);
                    player_obj.offsides = parseInt(data[i+8]);
                    player_obj.fouls = parseInt(data[i+9]);
                    player_obj.fouled = parseInt(data[i+10]);
                    player_obj.yellow_cards = parseInt(data[i+11]);
                    player_obj.red_card = parseInt(data[i+12]);
                    player_obj.start = parseInt(data[i+13]);
                    player_obj.sub = parseInt(data[i+14]);

                    game_arr.forEach((game)=>{
                        // console.log(game.espn_id);
                        if(game.espn_id === player_obj.game_id){
                            if(playerCnt % 35 <= 17){
                                if(game_obj.away_score){
                                    player_obj.goals_conceeded = game_obj.away_score;
                                    (game_obj.away_score === 0 ? player_obj.clean_sheet = 1 : player_obj.clean_sheet = 0);
                                }
                            }else{
                                if(game_obj.away_score){
                                    player_obj.goals_conceeded = game_obj.home_score;
                                    (game_obj.home_score === 0 ? player_obj.clean_sheet = 1 : player_obj.clean_sheet = 0);
                                }
                            }
                        }
                    });

                    for(var stat in player_obj){
                        if(player_obj[stat] === null)
                            player_obj[stat] = 0;
                    }
                    player_arr.push(player_obj);
                    i+=14;
                }
            }
        };

        var storeGames = async function(game){
            if(game){
                let precheck = await gamesModel.fetchGameEspn(game.espn_id);
                if(!precheck){
                    await gamesModel.saveGame(game);
                    let result = await gamesModel.fetchGameEspn(game.espn_id);
                    let scoreline = '<div class="score-row">';
                    scoreline += '<div class="home-score"><h1>' + result.home_score + '</h1></div>';
                    scoreline += '<div class="home-team" data-home="' + result.home_id + '"><h2>' + result.home_team + '</h2></div>';
                    scoreline += '<div class="away-team" data-away="' + result.away_id + '"><h2>' + result.away_team + '</h2></div>';
                    scoreline += '<div class="away-score"><h1>' + result.away_score + '</h1></div>';
                    scoreline += '</div>';
                    scoreline += '<div class="game-stats" data-game="' + result.espn_id + '">';
                    scoreline += '<table class="home-stats table-hover table-striped table-bordered table-responsive">';
                    scoreline += '<thead>';
                    scoreline += '<tr>';
                    scoreline += '<th colspan="15">Home Team: ' + result.home_team + '</th>';
                    scoreline += '</tr>';
                    scoreline += '<tr>';
                    scoreline += '<th>Last Name</th>';
                    scoreline += '<th>First Name</th>';
                    scoreline += '<th>Start</th>';
                    scoreline += '<th>Sub</th>';
                    scoreline += '<th>Position</th>';
                    scoreline += '<th>Goals</th>';
                    scoreline += '<th>Assists</th>';
                    scoreline += '<th>Saves</th>';
                    scoreline += '<th>Shots</th>';
                    scoreline += '<th>Shots On Target</th>';
                    scoreline += '<th>Offsides</th>';
                    scoreline += '<th>Fouls</th>';
                    scoreline += '<th>Fouled</th>';
                    scoreline += '<th>Yellow Cards</th>';
                    scoreline += '<th>Red Card</th>';
                    scoreline += '</tr>';
                    scoreline += '</thead>';
                    scoreline += '</table>';
                    scoreline += '<table class="away-stats table-hover table-striped table-bordered table-responsive">';
                    scoreline += '<thead>';
                    scoreline += '<tr>';
                    scoreline += '<th colspan="15">Away Team: ' + result.away_team + '</th>';
                    scoreline += '</tr>';
                    scoreline += '<tr>';
                    scoreline += '<th>Last Name</th>';
                    scoreline += '<th>First Name</th>';
                    scoreline += '<th>Start</th>';
                    scoreline += '<th>Sub</th>';
                    scoreline += '<th>Position</th>';
                    scoreline += '<th>Goals</th>';
                    scoreline += '<th>Assists</th>';
                    scoreline += '<th>Saves</th>';
                    scoreline += '<th>Shots</th>';
                    scoreline += '<th>Shots On Target</th>';
                    scoreline += '<th>Offsides</th>';
                    scoreline += '<th>Fouls</th>';
                    scoreline += '<th>Fouled</th>';
                    scoreline += '<th>Yellow Cards</th>';
                    scoreline += '<th>Red Card</th>';
                    scoreline += '</tr>';
                    scoreline += '</thead>';
                    scoreline += '</table>';
                    scoreline += '</div>';

                    retGames.push(scoreline);
                }
                await storeGames(game_arr.shift());
            }
        };

        var storePlayers = async function(player){
            if(player){
                let result = await gamesModel.savePlayerStats(player);
                // console.log(result);
                if(result !== 'Saved'){
                    let row = '<tr data-player="' + result.playerStats.player_id + '">';
                    row += '<td>' + result.playerStats.last_name + '</td>';
                    row += '<td>' + result.playerStats.first_name + '</td>';
                    row += '<td>' + result.gameStats.start + '</td>';
                    row += '<td>' + result.gameStats.sub + '</td>';
                    row += '<td>' + result.playerStats.position + '</td>';
                    row += '<td>' + result.gameStats.goals + '</td>';
                    row += '<td>' + result.gameStats.assists + '</td>';
                    row += '<td>' + result.gameStats.saves + '</td>';
                    row += '<td>' + result.gameStats.shots + '</td>';
                    row += '<td>' + result.gameStats.shots_on_target + '</td>';
                    row += '<td>' + result.gameStats.offsides + '</td>';
                    row += '<td>' + result.gameStats.fouls + '</td>';
                    row += '<td>' + result.gameStats.fouled + '</td>';
                    row += '<td>' + result.gameStats.yellow_cards + '</td>';
                    row += '<td>' + result.gameStats.red_card + '</td>';

                    let obj = {
                        game_id: result.gameStats.game_espn_id,
                        row: row
                    };

                    retPlayers.push(obj);
                }
                await storePlayers(player_arr.shift());
            }
        }
       
        console.log('Building Objects . . .');

        await createObj(Data);
        await storeGames(game_arr.shift());
        await storePlayers(player_arr.shift());

        let retObj = {};

        if(retGames.length > 0){
            retObj.games = retGames;
        }else{
            retObj.games = false;
        }

        if(retPlayers.length > 0){
            retObj.players = retPlayers;
        }else{
            retObj.players = false;
        }

        res.send(retObj);
    });
});

module.exports = router;