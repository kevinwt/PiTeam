var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session = require('express-session');
require('dotenv').config();


var games = require('./routes/games');
var index = require('./routes/index');
var users = require('./routes/users');
var players = require('./routes/players');
var admin = require('./routes/admin');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use('/stylesheets', express.static(__dirname + 'public/stylesheets'));
app.use('/javascript', express.static(__dirname + 'public/javascripts'));
// app.use('/python', express.static(__dirname + 'public/pythonscripts'));
app.use('/jquery', express.static(__dirname + '/node_modules/jquery/dist/'));
app.use('/fusioncharts', express.static(__dirname + '/node_modules/fusioncharts'));
app.use('/images', express.static(__dirname + 'public/images'));
app.use('/viewjs', express.static(__dirname + '/public/javascripts/views'));
app.use('/datatables', express.static(__dirname + '/node_modules/datatables.net/js'));
app.use('/datatables_responsive', express.static(__dirname + '/node_modules/datatables.net-responsive/js'));
app.use('/helper', express.static(__dirname + '/public/javascripts/helper'));
app.use('/bootstrap', express.static(__dirname + '/node_modules/bootstrap/dist'));
app.use('/datatables-bs', express.static(__dirname + '/node_modules/datatables.net-bs'));



// uncomment after placing your favicon in /public
app.use(favicon(path.join(__dirname, 'public/images', 'PiTeamFavicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(session({secret:process.env.SECRET_KEY, resave:false,saveUninitialized:true}));
app.use(express.static(path.join(__dirname, 'public')));

app.use('/games', games);
app.use('/players', players);
app.use('/users', users);
app.use('/admin', admin);
app.use('/', index);
//app.use('/users', users);

baseTemplate = function(req, res, next, viewFile, tmplFile){
  let username = 'TEST USER';
  console.log(req.session);
  if(req.session.user)
      username = req.session.user.username;

  res.render('defaultTemplate', {
      tmpl:tmplFile,
      view:viewFile,
      username:username
  });
};

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error',{
    error : err
  });
});

module.exports = app;
