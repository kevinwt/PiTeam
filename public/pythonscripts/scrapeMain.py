from gameScrape import getGameIDs
import playerScrape
import sys



urls = getGameIDs(sys.argv[1], sys.argv[2])
# urls = getGameIDs('20180414')
for game in urls:
    playerScrape.getGameInfo(game)