import requests
import re
from bs4 import BeautifulSoup 
import datetime
from sortedcontainers import SortedList
import sys, json

def hasDateFormat(tag):
	return not tag.has_attr('data-dateformat')

def getSchedule(start_date, end_date):
    date_url_base = "http://www.espn.com/soccer/fixtures/_/date/"
    
    start = datetime.datetime.strptime(start_date, "%Y/%m/%d")
    end = datetime.datetime.strptime(end_date, "%Y/%m/%d")
    current = start
    dates = []

    while current <= end:
        includeDate = datetime.datetime.strftime(current, "%Y%m%d")
        dates.append(includeDate)
        current = current + datetime.timedelta(days=1)

    EPL_header = None
    game_ids_check_list = []
    game_ids_list = []

    for index, date in enumerate(dates):

        html = requests.get(date_url_base + date).content
        soup = BeautifulSoup(html, 'html.parser')

        for index, h2_tag in enumerate(soup.find_all('h2')):

            if h2_tag.string == 'English Premier League':
                EPL_header = h2_tag
                break
            elif index >= 2:
                break

        if(EPL_header != None):
            sib = EPL_header.next_sibling

            for item in sib.find_all(hasDateFormat, href=re.compile("gameId")):
                if(not item.get('href').split('=')[1] in game_ids_check_list):
                    tempDate = str(datetime.datetime.strptime(date, "%Y%m%d"))
                    game_ids_check_list.append(item.get('href').split('=')[1])
                    game_ids_list.append({'game_id':item.get('href').split('=')[1], 'game_date':tempDate})

    summary_url_base = "http://www.espn.com/soccer/match?gameId="

    for index, obj in enumerate(game_ids_list):

        html = requests.get(summary_url_base + obj['game_id']).content
        soup_summary = BeautifulSoup(html, 'html.parser')

        home_id = None
        home_name = None
        away_id = None
        away_name = None
        home_score = '-'
        away_score = '-'
        game_time = None

        for article_tag in soup_summary.find_all('article'):
            for span in article_tag.find_all('span', {'data-behavior':'date_time'}):
                if not(span.has_attr('class')):
                    game_time = span['data-date']

        if(game_time != None):
            game_time = datetime.datetime.strptime(game_time, "%Y-%m-%dT%H:%MZ")
        else:
            game_time = obj['game_date']

        for index, team_wrap in enumerate(soup_summary.find_all('div', {'class':'team-info-wrapper'})):
            name = team_wrap.find('span', {'class':'short-name'})
            team_id_href = team_wrap.find('a', {'class':'team-name'})
            if(index == 0):
                home_name = name.string
                home_id = int(team_id_href['href'].split('/soccer/club/_/id/')[1])
            else:
                away_name = name.string
                away_id = int(team_id_href['href'].split('/soccer/club/_/id/')[1])
                
        if(soup_summary.find('span', {'data-home-away':'home', 'data-stat':'score'}).string != '\n'):
            home_score = int(soup_summary.find('span', {'data-home-away':'home', 'data-stat':'score'}).string)

        if(soup_summary.find('span', {'data-home-away':'away', 'data-stat':'score'}).string != '\n'):
            away_score = int(soup_summary.find('span', {'data-home-away':'away', 'data-stat':'score'}).string)
        

        print(json.dumps({
            'Game ID':obj['game_id'],
            'Home Score':home_score,
            'Home Team':home_name,
            'Home ID': home_id,
            'Away ID': away_id,
            'Away Team':away_name,
            'Away Score':away_score,
            'Game Time':datetime.datetime.strftime(game_time, '%Y-%m-%d %H:%M')
            }))

getSchedule(sys.argv[1], sys.argv[2])
# getSchedule('2018/12/01', '2018/12/11')