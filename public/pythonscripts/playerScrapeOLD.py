import requests
import re
from bs4 import BeautifulSoup 
from datetime import datetime, timedelta
from sortedcontainers import SortedList


class match(object):

    def __init__(self, game_id, game_time, home_id, away_id):
        self.espn_id = game_id
        self.game_time = game_time
        self.home_id = home_id
        self.away_id = away_id

    def score(self, home_score, away_score):
        self.home_score = home_score
        self.away_score = away_score


class player(object):

    def __init__(self, player_id, game_id):
        self.player_id = player_id
        self.game_id = game_id

    def outfield(self, goals, assists, shots, shotsOnTarget, cleanSheet, offsides, fouls, fouled, yellowCards, redCards):
        self.goals = goals
        self.assists = assists
        self.shots = shots
        self.shotsOnTarget = shotsOnTarget
        self.cleanSheet = cleanSheet
        self.offsides = offsides
        self.fouls = fouls
        self.fouled = fouled
        self.yellowCards = yellowCards
        self.redCards = redCards

    def goalie(self, saves, assists, shots, shotsOnTarget, cleanSheet, offsides, fouls, fouled, yellowCards, redCards):
        self.saves = saves
        self.assists = assists
        self.shots = shots
        self.shotsOnTarget = shotsOnTarget
        self.cleanSheet = cleanSheet
        self.offsides = offsides
        self.fouls = fouls
        self.fouled = fouled
        self.yellowCards = yellowCards
        self.redCards = redCards


def getGameInfo(id_date):

    id = id_date.split(' ')[0]
    game_date = id_date.split(' ')[1]

    lineups_url_base = "http://www.espn.com/soccer/lineups?gameId="
    summary_url_base = "http://www.espn.com/soccer/match?gameId="

    test_lineup_url = lineups_url_base + id
    test_summary_url = summary_url_base + id

    #print(test_lineup_url)
    #print(test_summary_url)

    summary_html_doc = requests.get(test_summary_url).content
    soup_summary = BeautifulSoup(summary_html_doc, 'lxml')

    lineup_html_doc = requests.get(test_lineup_url).content
    soup_lineup = BeautifulSoup(lineup_html_doc, 'lxml')

    #print(soup_lineup)

    home_id = None
    away_id = None
    game_time = None

    #GET THE TIME OF KICKOFF FOR THE MATCH
    for article_tag in soup_summary.find_all('article'):
        for span in article_tag.find_all('span', {'data-behavior':'date_time'}):
            if not(span.has_attr('class')):
                game_time = span['data-date']

    if(game_time != None):
        game_time = datetime.strptime(game_time, '%Y-%m-%dT%H:%MZ')
    else:
        game_time = datetime.strptime(game_date, '%Y%m%d')
    
    #GET THE ESPN ID FOR THE HOME AND AWAY TEAM
    for index, team_id in enumerate(soup_lineup.find_all('a', {"class" : "team-name"})):
        if(index % 2 == 0 and index < 3):
            t_ids = team_id['href']
            if(index == 0):
                home_id = t_ids.split("/").pop()
            else:
                away_id = t_ids.split("/").pop()

    #CREATE A MATCH OBJECT FOR THE GAME
    game = match(id, game_time, home_id, away_id)

    print('Game Object')
    print(game.espn_id)
    print(game.game_time)
    print(game.home_id)
    print(game.away_id)

    #IF THE GAME HAS BEEN PLAYED GET THE SCORE AND PLAYER STATS
    if(game_time < datetime.now()):
        print('Score')

        home_span = soup_lineup.find('span', {"data-home-away" : "home"})

        home_score = home_span.string.lstrip().rstrip()
        print(home_score)

        away_span = soup_lineup.find('span', {"data-home-away" : "away"})

        away_score = away_span.string.lstrip().rstrip()
        print(away_score)

        game.score(home_score, away_score)

        bench_list = []

        for test in soup_lineup.find_all('th', {"colspan" : "3"}):
            th_tag = test
            th_row = th_tag.parent
            thead_tag = th_row.parent
            bench_set = thead_tag.findNext('tbody')
            for plyr in bench_set.find_all('div', {"class" : "accordion-item"}):
                if(plyr["data-id"]):
                    bench_list.append(plyr["data-id"])

        #FOR EACH PLAYER IN THE ESPN TABLE GET THEIR STATS FOR THE MATCH
        for pid in soup_lineup.find_all('div', {"class" : "accordion-item"}):

            is_goalie = None
            player_id = None
            saves = None
            goals = None
            assists = None
            offsides = None
            shots = None
            shotsOnTarget = None
            fouls = None
            fouled = None
            yellowCards = None
            redCards = None
            sub = False
            sub_time = None

            if (pid["data-id"]):
                player_id = pid["data-id"]

                for span_tag in pid.find_all('span', {"class" : "icon-soccer-substitution-before"}):
                    for subtime in span_tag.find('span', {"class" : "detail"}):
                        sub = True
                        sub_time = subtime

                for span_tag in pid.find_all('span', {"class" : "value"}):
                    attr = span_tag["data-stat"]
                    if(attr == "totalGoals"):
                        goals = span_tag.string
                        is_goalie = False
                    elif(attr == "saves"):
                        saves = span_tag.string
                        is_goalie = True
                    elif(attr == "goalAssists"):
                        assists = span_tag.string
                    elif(attr == "offsides"):
                        offsides = span_tag.string
                    elif(attr == "yellowCards"):
                        yellowCards = span_tag.string
                    elif(attr == "redCards"):
                        redCards = span_tag.string

                for span_tag in pid.find_all('span', {"class" : "chartValue"}):
                    attr = span_tag["data-stat"]
                
                    if(attr == "shotsOnTarget"):
                        shotsOnTarget = span_tag.string
                    elif(attr == "totalShots"):
                        shots = span_tag.string
                    elif(attr == "foulsCommitted"):
                        fouls = span_tag.string
                    elif(attr == "foulsSuffered"):
                        fouled = span_tag.string
                    else:
                        print(attr + ": " + span_tag.string)

                if (not is_goalie):
                    print('Player')     #0
                    print(player_id)    #1
                    print(goals)        #2
                    print('0')          #3
                    print(assists)      #4
                    print(shots)        #5
                    print(shotsOnTarget)#6
                    print('0')          #7
                    print(offsides)     #8
                    print(fouls)        #9
                    print(fouled)       #10
                    print(yellowCards)  #11
                    print(redCards)     #12
                    if(player_id in bench_list):
                        print('0')      #13
                        print('0')      #14
                    elif(sub == True):    
                        print('0')      #13
                        print('1')      #14
                    else:
                        print('1')      #13
                        print('0')      #14
                else:
                    print('Goalie')
                    print(player_id)
                    print('0')
                    print(saves)
                    print(assists)
                    print(shots)
                    print(shotsOnTarget)
                    print('0')
                    print('0')
                    print(fouls)
                    print(fouled)
                    print(yellowCards)
                    print(redCards)
                    if(player_id in bench_list):
                        print('0')      #13
                        print('0')      #14
                    elif(sub == True):    
                        print('0')      #13
                        print('1')      #14
                    else:
                        print('1')      #13
                        print('0') 

    return 'Done'