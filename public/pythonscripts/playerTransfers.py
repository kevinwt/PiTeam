import requests
import re
from bs4 import BeautifulSoup 
from datetime import datetime, timedelta
from sortedcontainers import SortedList
import sys


url_base_0 = "http://www.espnfc.com/player/"
url_base_1 = "?season="


def getPlayerTeams(player_id, season_year):
    search_url = url_base_0 + player_id + url_base_1 + season_year

    season_html = requests.get(search_url).content
    soup_season = BeautifulSoup(season_html, 'lxml')


    if(soup_season):
        div_tag = soup_season.find('div', {'id' : 'player-appearances'})

        if(div_tag):
            table = div_tag.find('table', {'data-fixed-columns':'1'})


            cur_team = None
            last_date = None

            if(table):
                for row in table.find_all('tr'):
                    if(row):
                        comp_col = row.find('td', {'class' : 'comp'})
                        if(comp_col != None):
                            if(comp_col.string == 'Prem'):
                                team_col = row.find('td', {'class' : 'team'})
                                date_col = row.find('td', {'class' : 'date'})
                                if(cur_team == None):
                                        cur_team = team_col.string
                                elif(team_col.string != cur_team):
                                    print(cur_team + ' | ' + last_date)
                                    cur_team = team_col.string
                                    # print(team_col.string + ', ' + date_col.string)
                                last_date = date_col.string


                if(cur_team != None):
                    print(cur_team + ' | ' + last_date)
                else:
                    print('Out of League | ' + season_year)

            else:
                print('ERROR! NO SEASON TABLE FOUND!!!')
        
        else:
            print('ERROR! NO DIV TAG FOUND!!!')
    else:
        print('ERROR! NO PAGE FOUND!!!')
    # TEST
    # getPlayerTeams('92055', '2017')

getPlayerTeams(sys.argv[1], sys.argv[2])