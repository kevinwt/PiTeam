import requests
import re
from bs4 import BeautifulSoup 
import datetime
from sortedcontainers import SortedList
import sys

def tryMe(tag):
	return not tag.has_attr('data-dateformat')



def getGameIDs(start_date, end_date):
	print('Loading URLs')
	date_url_base = "http://www.espn.com/soccer/fixtures/_/date/"

	# end = datetime.datetime.now().strftime("%Y%m%d")
	end = datetime.datetime.strptime(end_date, "%Y%m%d")
	modified_date = datetime.datetime.strptime(start_date, "%Y%m%d")

	urls = []
	dates = []

	# print('start date: ' + start_date)
	# print('end date: ' + end_date)

	while modified_date <= end:
		format = datetime.datetime.strftime(modified_date, "%Y%m%d")
		# print(format)
		dates.append(format)

		urls.append(date_url_base + format)
		modified_date = modified_date + datetime.timedelta(days=1)

	datesOrderedURLs = SortedList(urls)

	#test = datesOrderedURLs[0]

	game_ids_list = []

	print('Loading Games . . .')
	for ind, day in enumerate(datesOrderedURLs):

		# print(day)

		dates_html_doc = requests.get(day).content
		soup = BeautifulSoup(dates_html_doc, 'html.parser')

		EPL_header = None
		game_ids_set = set()

		for index, header in enumerate(soup.find_all('h2')):

			if header.string == 'English Premier League':
				EPL_header = header
				# print(EPL_header)
				break
			elif index >= 5:
				break

		if(EPL_header != None):
			sib = EPL_header.next_sibling

			for item in sib.find_all(tryMe, href=re.compile("gameId")):
				game_ids_set.add(item.get('href').split('=')[1] + ' ' + dates[ind])

			game_ids_list.extend(SortedList(game_ids_set))

	#test_id = game_ids_list[0]

	# print(game_ids_list)

	return game_ids_list


# getGameIDs(sys.argv[1], sys.argv[2])
	