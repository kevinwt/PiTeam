import requests
import re
from bs4 import BeautifulSoup 
from datetime import datetime, timedelta
from sortedcontainers import SortedList
import sys, json

def playerStats(game_id):
    game_url = "http://www.espn.com/soccer/lineups?gameId=" + str(game_id)

    stats_page = requests.get(game_url).content
    soup_stats = BeautifulSoup(stats_page, 'html.parser')
    counter = 0
    starts_tracker = 0
    start_count = 0
    sub_count = 0

    for td_tag in soup_stats.find_all('td', {"colspan":"3"}):
        player = {
            'game_id': game_id,
            'id': None,
            'start': None,
            'sub': False,
            'goals': None,
            'saves': None,
            'assists': None,
            'shots': None,
            'shots_on_target': None,
            'fouls': None,
            'fouled': None,
            'offsides': None,
            'yellow_cards': None,
            'red_cards': None,
            'sub_time': None
        }
        
        for index, player_container in enumerate(td_tag.find_all('div',{'class':'accordion-item', "data-id":True})):

            player['id'] = player_container['data-id']
            counter += 1

            # print(index)

            if(counter == 18):
                starts_tracker = 0

            
            if(starts_tracker < 11 and index == 0):
                player['start'] = True
                starts_tracker = starts_tracker + 1
            else:
                player['start'] = False

            if(player_container.find('span', {'data-stat':'saves'}) != None):
                player['saves'] = player_container.find('span', {'data-stat':'saves'}).string

            if(player_container.find('span', {'data-stat':'totalGoals'}) != None):
                player['goals'] = player_container.find('span', {'data-stat':'totalGoals'}).string

            if(player_container.find('span', {'data-stat':'totalShots'}) != None):
                player['shots'] = player_container.find('span', {'data-stat':'totalShots'}).string

            if(player_container.find('span', {'data-stat':'shotsOnTarget'}) != None):
                player['shots_on_target'] = player_container.find('span', {'data-stat':'shotsOnTarget'}).string

            if(player_container.find('span', {'data-stat':'foulsCommitted'}) != None):
                player['fouls'] = player_container.find('span', {'data-stat':'foulsCommitted'}).string

            if(player_container.find('span', {'data-stat':'foulsSuffered'}) != None):
                player['fouled'] = player_container.find('span', {'data-stat':'foulsSuffered'}).string

            if(player_container.find('span', {'data-stat':'goalAssists'}) != None):
                player['assists'] = player_container.find('span', {'data-stat':'goalAssists'}).string

            if(player_container.find('span', {'data-stat':'offsides'}) != None):
                player['offsides'] = player_container.find('span', {'data-stat':'offsides'}).string

            if(player_container.find('span', {'data-stat':'yellowCards'}) != None):
                player['yellow_cards'] = player_container.find('span', {'data-stat':'yellowCards'}).string

            if(player_container.find('span', {'data-stat':'redCards'}) != None):
                player['red_cards'] = player_container.find('span', {'data-stat':'redCards'}).string

            if(player_container.find('span', {'class':'icon-soccer-substitution-before'}) != None):
                player['sub'] = True
                sub_tag = player_container.find('span', {'class':'icon-soccer-substitution-before'})
                if(sub_tag.find('span', {'class':'detail'}).string != None):
                    player['sub_time'] = sub_tag.find('span', {'class':'detail'}).string
                else:
                    print('Error: Sub tag found but no sub time!!!')

            if(player['start'] == True):
                start_count = start_count + 1
            elif(player['sub'] == True):
                sub_count = sub_count + 1

            print(json.dumps(player))
            # print(index)


    # print(counter)
    # print(start_count)
    # print(sub_count)

# playerStats('513721')
# playerStats('513720')

playerStats(sys.argv[1])