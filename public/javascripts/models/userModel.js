const sqlite3 = require('sqlite3').verbose();
var Promise = require('bluebird');
var dbHelper = require('./../helper/dbHelper');
var moment = require('moment');

function openDB(){
    let db = dbHelper.openDB(__dirname + '/../../../databases/piteam.db');
    return db;
}

function allUsers(){
    let db = openDB();
    let usersAll = 'SELECT u.*, ak.key_name FROM user u, access_keys ak WHERE u.access = ak.id';
    return dbHelper.asyncAll(db, usersAll, [], true);
}

function getUser(id){
    let db = openDB();
    let usersGet = 'SELECT * FROM user WHERE id = ? LIMIT 1';
    return dbHelper.asyncGet(db, usersGet, [id], true);
}

function allAccessKeys(){
    let db = openDB();
    let accessAll = 'SELECT * FROM access_keys';
    return dbHelper.asyncAll(db, accessAll, [], true);
}

function newUser(userInfo){
    let db = openDB();
    let addUser = 'INSERT INTO user (id, first_name, last_name, username, password, access, email) VALUES (NULL, ?, ?, ?, ?, ?, ?)';
    let getUser = 'SELECT u.username FROM user u WHERE u.id = (SELECT MAX(id) FROM user) LIMIT 1';
    dbHelper.asyncRun(db, addUser, [userInfo.first_name, userInfo.last_name, userInfo.username, userInfo.hash, userInfo.access, userInfo.email], false);
    return dbHelper.asyncGet(db, getUser, [], true);
}

function deleteUser(userId){
    let db = openDB();
    let deleteUser = 'DELETE FROM user WHERE id = ?';
    return dbHelper.asyncRun(db, deleteUser, [userId], true);
}

async function updateUser(userInfo){
    let db = openDB();
    let updateUser = 'UPDATE user SET first_name = ?, last_name = ?, username = ?, email = ?, access = ? WHERE id = ?';
    let selectUser = 'SELECT username FROM user WHERE id = ? LIMIT 1';
    let params = [userInfo.first_name, userInfo.last_name, userInfo.username, userInfo.email, userInfo.access];
    if(userInfo.password && userInfo.password !== ''){
        updateUser = 'UPDATE user SET first_name = ?, last_name = ?, username = ?, email = ?, access = ?, password = ? WHERE id = ?';
        params = params.concat([userInfo.hash]);
    }

    // console.log(selectUser);
    console.log(updateUser);
    console.log(params);

    await dbHelper.asyncRun(db, updateUser, params.concat([userInfo.id]), false);
    return await dbHelper.asyncGet(db, selectUser, [userInfo.id], true);
}

async function userDashTeamsLeagues(userId){
    let db = openDB();
    let curDatetime = moment().format('YYYY/MM/DD HH:mm');
    let getWeek = 'SELECT week_num FROM week w WHERE w.id = (SELECT MIN(id) FROM week w2 WHERE w2.end >= ? ORDER BY w2.week_num ASC) LIMIT 1';
    let getLeagues = 'SELECT l.* FROM league_participants lp, league l WHERE lp.user_id = ? AND lp.league_id = l.id LIMIT 3';
    let getTeams = 'SELECT ftg.* FROM fantasy_team_general ftg, league l WHERE ftg.user_id = ? AND ftg.league_id = l.id LIMIT 3';
    let getCapts = 'SELECT c.*, ftg.league_id FROM captain c, fantasy_team_general ftg WHERE ftg.user_id = ? AND c.team_id = ftg.id AND c.week = ? LIMIT 3';
    let getTrans = 'SELECT t.*, ftg.league_id FROM fantasy_transfers t, fantasy_team_general ftg WHERE ftg.user_id = ? AND ftg.id = t.team_id AND t.week = ? LIMIT 9';

    try{
        let week = await dbHelper.asyncGet(db, getWeek, [curDatetime], false);
        let leagues = await dbHelper.asyncAll(db, getLeagues, [userId], false);
        let teams = await dbHelper.asyncAll(db, getTeams, [userId], false);
        let capts = await dbHelper.asyncAll(db, getCapts, [userId, week.week_num], false);
        let trans = await dbHelper.asyncAll(db, getTrans, [userId, week.week_num], true);
        return {
            leagues: leagues,
            teams: teams,
            capts: capts,
            trans: trans
        };
    }catch(e){
        return e;
    }
}

function getUserLoginByUsername(username){
    let db = openDB();
    let getUserInfo = 'SELECT u.*, ak.key_name FROM user u, access_keys ak WHERE username = ? AND u.access = ak.id';
    return dbHelper.asyncAll(db, getUserInfo, [username], true);
}

function checkLeagueTaken(leagueName, password){
    let db = openDB();
    let getLeagueCheck = 'SELECT * FROM league WHERE league_name = ? AND passcode = ?';
    return dbHelper.asyncGet(db, getLeagueCheck, [leagueName, password], true);
}

async function createNewLeague(leagueName, userId, password, teamName){
    let db = openDB();
    let insertNewLeague = 'INSERT INTO league (id, league_name, owner, passcode) VALUES (NULL, ?, ?, ?)';
    let insertLeaguePart = 'INSERT INTO league_participants (id, league_id, user_id) VALUES (NULL, ?, ?)';
    let insertTeamName = 'INSERT INTO fantasy_team_general (id, league_id, user_id, team_name) VALUES (NULL, ?, ?, ?)';
    let getNewLeague = 'SELECT * FROM league WHERE id = (SELECT MAX(id) FROM league)';
    await dbHelper.asyncRun(db, insertNewLeague, [leagueName, userId, password], false);
    let leagueDetails = await dbHelper.asyncGet(db, getNewLeague, [], false);
    dbHelper.asyncRun(db, insertTeamName, [leagueDetails.id, userId, teamName], false);
    dbHelper.asyncRun(db, insertLeaguePart, [leagueDetails.id, userId], true);
    return leagueDetails;
}

async function setTransfer(trans_obj){
    let db = openDB();
    let curDatetime = moment().format('YYYY/MM/DD HH:mm');
    let getWeek = 'SELECT week_num FROM week w WHERE AND w.id = (SELECT MIN(id) FROM week w2 WHERE w2.end >= ? ORDER BY w2.week_num) LIMIT 1';

    let week = await asyncGet(db, getWeek, [curDatetime], false);
    if(trans_id == 0){
        let transQuery = 'INSERT INTO fantasy_transfers (id, team_id, player_out, price_out, player_in, price_in, date, week)' + 
                         'VALUES (NULL, ?, ?, ?, ?, ?, ?, ?)';
        let params = []
        let select = 'SELECT * FROM fantasy_transfers t WHERE t.id = (SELECT MAX(id) FROM fantasy_transfers) LIMIT 1';
        let selectParams = [];
    }else{
        let transQuery = 'UPDATE fantasy_transfers SET team_id = ?, player_out = ?, price_out = ?, player_in = ?, price_in = ?,' +
                         'date = ?, week = ? WHERE id = ?';
        let transParams = [];
        let select = 'SELECT * FROM fantasy_transfers WHERE id = ? LIMIT 1';
        let selectParams = [trans_id];
    }
    await dbHelper.asyncRun(db, transQuery, transParams, false);
    return await dbHelper.asyncGet(db, select, selectParams, true);
}

async function getUserTransfer(user_id, trans_id){
    let db = openDB();
    let getTrans = 'SELECT t.* FROM fantasy_transfers t, fantasy_team_general ft WHERE t.id = ? AND t.team_id = ft.id AND ft.user_id = ? LIMIT 1';
    return await dbHelper.asyncGet(db, getTrans, [trans_id, user_id], true);
}

async function getWeek(date){
    let db = openDB();
    if(!date){
        date = moment().format('YYYY/MM/DD HH:mm');
    }
    let getWeek = 'SELECT week_num FROM week w WHERE w.id = (SELECT MIN(id) FROM week w2 WHERE w2.end >= ? ORDER BY w2.week_num) LIMIT 1';
    return await dbHelper.asyncGet(db, getWeek, [date], true);
}

async function getFantasyTeam(userObj){
    let db = openDB();
    let getTeamGeneral = 'SELECT * FROM fantasy_team_general ftg WHERE ftg.id = ? AND ftg.user_id = ? LIMIT 1';
    return await dbHelper.asyncGet(db, getTeamGeneral, [userObj.team_id, userObj.user_id], true);
}

async function getFantasyTeamPlayers(teamObj){
    let db = openDB();
    let getTeamPlayers = 'SELECT * FROM fantasy_team_players ftp, prem_league_players plp WHERE plp.player_id = ftp.player_id AND ftp.team_id = ? AND ftp.user_id = ? LIMIT 11';
    return await dbHelper.asyncGet(db, getTeamPlayers, [teamObj.team_id, teamObj.user_id], true);
}

async function getUserTransfersWeek(transObj){
    let db = openDB();
    let getTransfers =  'SELECT * FROM fantasy_transfers ft WHERE ft.team_id = ? AND ft.week = ? LIMIT 3';
    return await dbHelper.asyncAll(db, getTransfers, [transObj.team_id, transObj.week], true);
}

async function getGeneralPlayerTable(){
    let db = openDB();
    let getTable = 'SELECT p.player_id, p.first_name || " " || p.last_name AS name, t.team_abv, p.position, p.cost FROM prem_league_players p, prem_league_teams ' +
                   't WHERE t.team_id = p.team_id AND t.in_prem = 1 ORDER BY t.team_abv';
    return await dbHelper.asyncAll(db, getTable, [], true);
}



module.exports.allUsers = allUsers;
module.exports.getUser = getUser;
module.exports.allAccessKeys = allAccessKeys;
module.exports.newUser = newUser;
module.exports.deleteUser = deleteUser;
module.exports.updateUser = updateUser;
module.exports.userDashTeamsLeagues = userDashTeamsLeagues;
module.exports.getUserLoginByUsername = getUserLoginByUsername;


module.exports.checkLeagueTaken = checkLeagueTaken;
module.exports.createNewLeague = createNewLeague;
module.exports.setTransfer = setTransfer;
module.exports.getWeek = getWeek;
module.exports.getFantasyTeam = getFantasyTeam;
module.exports.getFantasyTeamPlayers = getFantasyTeamPlayers;
module.exports.getUserTransfer = getUserTransfer;
module.exports.getUserTransfersWeek = getUserTransfersWeek;
module.exports.getGeneralPlayerTable = getGeneralPlayerTable;