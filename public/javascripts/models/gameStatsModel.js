const sqlite3 = require('sqlite3').verbose();
var Promise = require('bluebird');
var dbHelper = require('./../helper/dbHelper');

function openDB(){
    let db = dbHelper.openDB(__dirname + '/../../../databases/piteam.db');
    return db;
}

function fetchGameStats(game_id){
    let db = openDB();
    let gameStatsGet = 'SELECT t1.team_name AS home_team, t1.team_id AS home_team_id, t2.team_name AS away_team, t2.team_id AS away_team_id, g.* '
                     + 'FROM prem_league_games g, prem_league_teams t1, prem_league_teams t2 '
                     + 'WHERE t1.espn_id = g.home_id AND t2.espn_id = g.away_id AND g.game_id = ?';
    return dbHelper.asyncGet(db, gameStatsGet, [game_id], true);
}

function fetchGameTime(espn_id){
    let db = openDB();
    let gameTimeGet = 'SELECT g.game_time FROM prem_league_games g WHERE g.espn_id = ? LIMIT 1';
    return dbHelper.asyncGet(db, gameTimeGet, [espn_id], true);
}



module.exports.fetchGameStats = fetchGameStats;
module.exports.fetchGameTime = fetchGameTime;
