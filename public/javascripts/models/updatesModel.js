const sqlite3 = require('sqlite3').verbose();
var Promise = require('bluebird');
var dbHelper = require('./../helper/dbHelper');
var moment = require('moment');

function openDB(){
    let db = dbHelper.openDB(__dirname + '/../../../databases/piteam.db');
    return db;
}

function teams_dropdown(){
    let db = openDB();
    return dbHelper.asyncAll(db, "SELECT team_id, team_name FROM prem_league_teams", true);
}

function playerSearch(player){
    if(player.first_name === undefined || player.first_name === null)
        player.first_name = '';
    let db = openDB();
    var query = 'SELECT p.player_id, p.espn_id, p.first_name, p.last_name, t.team_name, p.position FROM prem_league_players p, prem_league_teams t WHERE p.first_name = ? AND p.last_name = ? AND p.team_id = t.team_id';
    return dbHelper.asyncAll(db, query, [player.first_name, player.last_name], true);
}

function allPlayers(){
    let db = dbHelper.openDB(__dirname + '/../../../databases/players.db');
    var query = 'SELECT p.player_id, p.espn_id, p.first_name, p.last_name, p.cost, t.team_name, p.position, p.team_id, p.injured, t.team_abv, t.transfer_abv FROM prem_league_players p, prem_league_teams t WHERE p.team_id = t.team_id ORDER BY t.team_id, p.position';
    return dbHelper.asyncAll(db, query, [], true);
}

function addPlayer(player){
    if(!player.espn)
        player.espn = '';
    if(!player.first)
        player.first = '';
    
    let db = openDB();
    let query1 = 'INSERT INTO prem_league_players (player_id, first_name, last_name, cost, team_id, position, injured, espn_id) VALUES (NULL, ?, ?, ?, ?, ?, ?, ?)';
    // let query2 = 'SELECT p.player_id, p.first_name, p.last_name, p.position, p.espn_id, p.team_id, p.cost, p.injured, t.team_name FROM prem_league_players p, prem_league_teams t WHERE first_name = ? AND last_name = ? AND position = ? AND p.team_id = ? AND p.team_id = t.team_id LIMIT 1';
    let query2 = 'SELECT p.player_id, p.first_name as first, p.last_name as last, p.position, p.espn_id, p.team_id, p.cost, p.injured, t.team_name as team FROM prem_league_players p, prem_league_teams t WHERE p.player_id = (SELECT MAX(player_id) FROM prem_league_players) AND p.team_id = t.team_id LIMIT 1';
    dbHelper.asyncRun(db, query1, [player.first, player.last, player.cost, player.team, player.position, player.injured, player.espn], false);
    return dbHelper.asyncGet(db, query2, [], true);
}

async function playerUpdate(playerUpd){
    let queryPlayer = 'SELECT p.player_id, p.first_name as first, p.last_name as last, p.cost, p.team_id, p.position, p.injured, p.espn_id, t.team_name as team, t.in_prem FROM prem_league_players p, prem_league_teams t WHERE p.team_id = t.team_id AND p.player_id = ? LIMIT 1';
    let paramsPlayer = [playerUpd.player_id];
    let db = openDB();

    return new Promise(async function(resolve, reject){      
        if(!playerUpd.espn)
            playerUpd.espn = '';
        if(!playerUpd.first)
            playerUpd.first = '';
        if(playerUpd.injured === 'true')
            playerUpd.injured = 1;
        else
            playerUpd.injured = 0;

        //Format date for entry
        playerUpd.date = playerUpd.date.split('/');
        if(playerUpd.date.length === 3){
            let tempDate = playerUpd.date[0] + '-' + playerUpd.date[1] + '-' + playerUpd.date[2];
            playerUpd.date = tempDate;
        }else{
            console.error('Date Format Invalid!');
        }

        var player = await dbHelper.asyncGet(db, queryPlayer, paramsPlayer, false);

        if(player.team_id === null)
            player.team_id = '';
        if(player.cost === null)
            player.cost = 0;
        if(player.position === null)
            player.position = '';

        if(player.team_id.toString() !== playerUpd.team.toString()){
            let queryTeam = ['UPDATE prem_league_players SET team_id = ? WHERE player_id = ?', 'INSERT INTO prem_league_transfer (id, player_id, new_team_id, date_changed) VALUES (NULL, ?, ?, ?)'];
            let paramsTeam = [[playerUpd.team, playerUpd.player_id], [playerUpd.player_id, playerUpd.team, playerUpd.date]];
            await dbHelper.asyncRun(db, queryTeam[0], paramsTeam[0], false);
            dbHelper.asyncRun(db, queryTeam[1], paramsTeam[1], false);
        }
    
        if(player.cost.toString() !== playerUpd.cost.toString()){
            let queryCost = ['UPDATE prem_league_players SET cost = ? WHERE player_id = ?', 'INSERT INTO prem_league_price (id, player_id, cost, date_changed) VALUES (NULL, ?, ?, ?)'];
            let paramsCost = [[playerUpd.cost, playerUpd.player_id], [playerUpd.player_id, playerUpd.cost, playerUpd.date]];
            await dbHelper.asyncRun(db, queryCost[0], paramsCost[0], false);
            dbHelper.asyncRun(db, queryCost[1], paramsCost[1], false);
        }
    
        if(player.position !== playerUpd.position){
            let queryPosition = ['UPDATE prem_league_players SET position = ? WHERE player_id = ?', 'INSERT INTO prem_league_position_change (id, player_id, new_position, date_changed) VALUES (NULL, ?, ?, ?)'];
            let paramsPosition = [[playerUpd.position, playerUpd.player_id], [playerUpd.player_id, playerUpd.position, playerUpd.date]];
            await dbHelper.asyncRun(db, queryPosition[0], paramsPosition[0], false);
            dbHelper.asyncRun(db, queryPosition[1], paramsPosition[1], false);
        }

        let queryGeneral = 'UPDATE prem_league_players SET first_name = ?, last_name = ?, espn_id = ?, injured = ? WHERE player_id = ?';
        let paramsGeneral = [playerUpd.first, playerUpd.last, playerUpd.espn, playerUpd.injured, playerUpd.player_id];
        await dbHelper.asyncRun(db, queryGeneral, paramsGeneral, false);
        
        player = await dbHelper.asyncGet(db, queryPlayer, paramsPlayer, true)
        return resolve(player);
    });
}

function transferHistory(player_id){
    let db = dbHelper.openDB(__dirname + '/../../../databases/players.db');
    let allPlayersQuery = 'SELECT TR.*, T.team_name, T.transfer_abv FROM prem_league_transfer TR, prem_league_teams T WHERE TR.new_team_id = T.team_id AND TR.player_id = ? ORDER BY date_changed';
    return dbHelper.asyncAll(db, allPlayersQuery, [player_id], true);
    //Returns from transfer table (id, player_id (table), new_team_id (table), date_changed)
    //Returns from team table (team_name, transfer_abv);
}

function allTeams(){
    let db = dbHelper.openDB(__dirname + '/../../../databases/players.db');
    let allPlayersQuery = 'SELECT * FROM prem_league_teams';
    return dbHelper.asyncAll(db, allPlayersQuery, [player_id], true);
}

function premYear(year){
    let db = dbHelper.openDB(__dirname + '/../../../databases/players.db');
    let yearQuery = 'SELECT * FROM prem_league_season_dates WHERE year = ?';
    return dbHelper.asyncAll(db, yearQuery, [year], true);
}

function tranInfo(espn_abv){
    let db = dbHelper.openDB(__dirname + '/../../../databases/players.db');
    let transferQuery = 'SELECT team_id, espn_id, team_name, team_abv, transfer_abv FROM prem_league_teams WHERE transfer_abv = ? LIMIT 1';
    return dbHelper.asyncGet(db, transferQuery, [espn_abv], true);
}

async function insertNewTransfer(transfer_info){
    // console.log(transfer_info);
    let db = dbHelper.openDB(__dirname + '/../../../databases/players.db');
    let transferInsert = 'INSERT INTO prem_league_transfer (id, player_id, new_team_id, date_changed) VALUES (NULL, ?, ?, ?)';
    let transferSelect = 'SELECT * FROM prem_league_transfer WHERE id = (SELECT MAX(TR.id) FROM prem_league_transfer TR)';
    await dbHelper.asyncRun(db, transferInsert, [transfer_info.player_id, transfer_info.team_id, transfer_info.date], false);
    return dbHelper.asyncGet(db, transferSelect, [], true);
}


module.exports.teams_dropdown = teams_dropdown;
module.exports.playerSearch = playerSearch;
module.exports.allPlayers = allPlayers;
module.exports.addPlayer = addPlayer;
module.exports.playerUpdate = playerUpdate;
module.exports.transferHistory = transferHistory;
module.exports.allTeams = allTeams;
module.exports.premYear = premYear;
module.exports.tranInfo = tranInfo;
module.exports.insertNewTransfer = insertNewTransfer;