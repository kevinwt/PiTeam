const sqlite3 = require('sqlite3').verbose();
var Promise = require('bluebird');
var dbHelper = require('./../helper/dbHelper');


function openDB(){
    let db = dbHelper.openDB(__dirname + '/../../../databases/piteam.db');
    return db;
}

function teamName(teamId){
    let teamQuery = 'SELECT team_name FROM prem_league_teams WHERE espn_id = ? LIMIT 1';
    let db = openDB();
    return dbHelper.asyncGet(db, teamQuery, [teamId], true)
}

// Used in scheduled python scrape to save general game info (uses espn game id)
async function saveGame(game){
    let db = openDB();
    let gameQuery = 'INSERT INTO prem_league_games (game_id, espn_id, home_id, away_id, home_score, away_score, game_time) VALUES (NULL, ?, ?, ?, ?, ?, ?)';
    return await dbHelper.asyncRun(db, gameQuery, [game['Game ID'], game['Home ID'], game['Away ID'], game['Home Score'], game['Away Score'], game['Game Time']], true);
}

// Used in scheduled python scrape to update general game info (uses espn game id)
async function updateGame(game){
    let db = openDB();
    let gameQuery = 'UPDATE prem_league_games SET home_id = ?, away_id = ?, home_score = ?, away_score = ?, game_time = ? WHERE espn_id = ?';
    return await dbHelper.asyncRun(db, gameQuery, [game['Home ID'], game['Away ID'], game['Home Score'], game['Away Score'], game['Game Time'], game['Game ID']], true);
}

async function fetchGameEspn(espn_id){
    let db = openDB();
    let gameQuery = 'SELECT G.game_id, G.espn_id, G.home_id, G.away_id, G.home_score, G.away_score, G.game_time, t1.team_name as home_team, t2.team_name as away_team FROM prem_league_games G, prem_league_teams t1, prem_league_teams t2 WHERE G.espn_id = ? AND G.home_id = t1.espn_id AND G.away_id = t2.espn_id LIMIT 1';
    return await dbHelper.asyncGet(db, gameQuery, [espn_id], true);
}

async function savePlayerStats(player){
    // console.log(player);
    let db = openDB();

    let savedCheck = 'SELECT * FROM prem_league_player_stats WHERE game_espn_id = ? AND player_espn_id = ?';

    let playerInsert = 'INSERT INTO prem_league_player_stats (id, game_espn_id, player_espn_id, start, sub, goals, assists, clean_sheet, goals_conceeded, red_card, saves, shots, shots_on_target, fouls, fouled, offsides, yellow_cards) VALUES (NULL, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)';
    let playerSelect = 'SELECT * FROM prem_league_players P WHERE P.espn_id = ? LIMIT 1';
    let playerUpdate = 'UPDATE prem_league_players SET starts = ?, subs = ?, goals = ?, assists = ?, clean_sheets = ?, goals_conceeded = ?, red_cards = ?, saves = ?, shots = ?, shots_on_target = ?, fouls = ?, fouled = ?, offsides = ?, yellow_cards = ? WHERE espn_id = ?';
    let playerSelect2 = 'SELECT S.id, S.game_espn_id, S.player_espn_id, S.start, S.sub, S.goals, S.assists, S.clean_sheet, S.goals_conceeded, S.red_card, S.saves, S.shots, S.shots_on_target, S.fouls, S.fouled, S.offsides, S.yellow_cards, P.first_name, P.last_name FROM prem_league_player_stats S, prem_league_players P WHERE S.game_espn_id = ? AND S.player_espn_id = ? AND P.espn_id = ? LIMIT 1';

    let playerRow = await dbHelper.asyncGet(db, playerSelect, [player.id], false);


    let precheck = await dbHelper.asyncGet(db, savedCheck, [player.game_id, player.id], false);

    if(!precheck){
        if(!playerRow){
            let rowInsert = 'INSERT INTO prem_league_players (player_id, espn_id, team_id, first_name, last_name, position, cost, starts, subs, goals, assists, clean_sheets, goals_conceeded, red_cards, saves, shots, shots_on_target, fouls, fouled, offsides, yellow_cards, injured, pens_missed) VALUES (NULL, ?, 500, "UNKNOWN", "UNKNOWN", NULL, NULL, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, 0, 0)';
            await dbHelper.asyncRun(db, rowInsert, [player.id, player.start, player.sub, player.goals, player.assists, player.clean_sheet, player.goals_conceeded, player.red_card, player.saves, player.shots, player.shots_on_target, player.fouls, player.fouled, player.offsides, player.yellow_cards], false);

            playerRow = await dbHelper.asyncGet(db, playerSelect, [player.id], false);
        }

        if(player.start === 1){
            playerRow.starts++;
        }else if(player.sub === 1){
            playerRow.subs++;
        }
        
        playerRow.goals += player.goals;
        playerRow.assists += player.assists;

        if(player.clean_sheet)
            playerRow.clean_sheets++;
        else
            playerRow.goals_conceeded += player.goals_conceeded;

        if(player.red_card)
            playerRow.red_cards++;
        
        playerRow.saves += player.saves;
        playerRow.shots += player.shots;
        playerRow.shots_on_target += player.shots_on_target;
        playerRow.fouls += player.fouls;
        playerRow.fouled += player.fouled;
        playerRow.offsides += player.offsides;
        playerRow.yellow_cards += player.yellow_cards;

        if(playerRow.position === 'MID' || playerRow.position === 'FOR'){
            playerRow.saves = 0;
            player.saves = 0;
        }

        await dbHelper.asyncRun(db, playerInsert, [player.game_id, player.id, player.start, player.sub, player.goals, player.assists, player.clean_sheet, player.goals_conceeded, player.red_card, player.saves, player.shots, player.shots_on_target, player.fouls, player.fouled, player.offsides, player.yellow_cards], false);
        await dbHelper.asyncRun(db, playerUpdate, [playerRow.starts, playerRow.subs, playerRow.goals, playerRow.assists, playerRow.clean_sheets, playerRow.goals_conceeded, playerRow.red_cards, playerRow.saves, playerRow.shots, playerRow.shots_on_target, playerRow.fouls, playerRow.fouled, playerRow.offsides, playerRow.yellow_cards, player.id], false);
        let gameStats = await dbHelper.asyncGet(db, playerSelect2, [player.game_id, player.id, player.id], false);
        // console.log(playerSelect2);
        // console.log([player.game_id, player.id, player.id]);
        let playerStats = await dbHelper.asyncGet(db, playerSelect, [player.id], true);

        let obj = {
            name: playerStats.first_name + ' ' + playerStats.last_name,
            gameStats: gameStats,
            playerStats: playerStats
        };

        return obj;
    }else{
        dbHelper.closeDB(db);
        // console.log('Game already saved.');
        return 'Saved';
    }
}



async function fetchAllPremGames(){
    let db = openDB();
    let gameQuery = 'SELECT * FROM prem_league_games';
    return await dbHelper.asyncAll(db, gameQuery, [], true);
}

module.exports.teamName = teamName;
module.exports.saveGame = saveGame;
module.exports.updateGame = updateGame;
module.exports.savePlayerStats = savePlayerStats;
module.exports.fetchGameEspn = fetchGameEspn;

module.exports.fetchAllPremGames = fetchAllPremGames;