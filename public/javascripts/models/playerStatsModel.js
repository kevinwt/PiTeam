const sqlite3 = require('sqlite3').verbose();
var Promise = require('bluebird');
var dbHelper = require('./../helper/dbHelper');

function openDB(){
    let db = dbHelper.openDB(__dirname + '/../../../databases/piteam.db');
    return db;
}

function FetchAllEspnId(){
    let db = openDB();
    let espnAll = 'SELECT espn_id FROM prem_league_players';
    return dbHelper.asyncAll(db, espnAll, [], true);
}

function fetchEspnIdByPlayerId(player_id){
    let db = openDB();
    let espnGet = 'SELECT espn_id FROM prem_league_players WHERE player_id = ? LIMIT 1';
    return dbHelper.asyncGet(db, espnGet, [player_id], true);
}

function fetchPlayerStatsForSeason(player_id, year){
    let db = openDB();
    let seasonStatsAll = 'SELECT p.player_id, p.first_name, p.last_name, t1.team_name AS home_team, t2.team_name AS away_team, t.team_abv, ps.*, g.* '
                    + 'FROM prem_league_players p, prem_league_player_stats ps, prem_league_teams t1, prem_league_teams t2, prem_league_games g, prem_league_season_dates s '
                    + 'WHERE p.player_id = ? AND p.espn_id = ps.player_espn_id AND ps.game_espn_id = g.espn_id AND g.home_id = t1.espn_id AND '
                        + 'g.away_id = t2.espn_id AND s.year = ? AND g.game_time >= s.start_date AND g.game_time <= s.end_date ORDER BY g.game_time';
    return dbHelper.asyncAll(db, seasonStatsAll, [player_id, year], true);
}

function fetchSingleGamePlayerStats(game_id){
    let db = openDB();
    let gameStatsAll = 'SELECT ps.*, p.player_id, p.espn_id, p.first_name, p.last_name '
                     + 'FROM prem_league_player_stats ps, prem_league_players p '
                     + 'WHERE ps.game_espn_id = ? AND p.espn_id = ps.player_espn_id';
    return dbHelper.asyncAll(db, gameStatsAll, [game_id], true);
}

function fetchAllPlayerTransfers(player_id){
    let db = openDB();
    let transfersAll = 'SELECT tr.*, t.team_name FROM prem_league_transfer tr, prem_league_teams t WHERE tr.player_id = ? AND tr.new_team_id = t.team_id ORDER BY tr.date_changed DESC';
    return dbHelper.asyncAll(db, transfersAll, [player_id], true);
}

function fetchActiveTeamOnDate(player_id, date){
    let db = openDB();
    let activeTeamGet = 'SELECT * FROM prem_league_transfer WHERE date_changed = (SELECT MAX(tr.date_changed) FROM prem_league_transfer tr '
                      + 'WHERE tr.player_id = ? AND tr.date_changed < ?) AND player_id = ? LIMIT 1';
    return dbHelper.asyncGet(db, activeTeamGet, [player_id, date, player_id], true);
}

function fetchActiveTeamsForSeason(player_id, year){
    let db = openDB();
    let activeTeamGet = 'SELECT * FROM prem_league_transfer tr WHERE date_changed <= (SELECT end_date FROM prem_league_season_dates WHERE year = ?) AND player_id = ?';
    return dbHelper.asyncGet(db, activeTeamGet, [year, year, player_id], true);
}

function fetchLastFourGames(player_id, date){
    let db = openDB();
    let lastFourGet = 'SELECT ps.*, p.player_id, p.espn_id, p.first_name, p.last_name, g.game_time '
                    + 'FROM prem_league_player_stats ps, prem_league_players p, prem_league_games g '
                    + 'WHERE ps.player_espn_id = ? AND p.espn_id = ps.player_espn_id AND g.espn_id = ps.game_espn_id '
                    + 'AND g.game_time < ? ORDER BY g.game_time LIMIT 4';
    return dbHelper.asyncGet(db, lastFourGet, [player_id, date], true);
}

async function fetchHomeStats(player_id, year){
    let db = openDB();
    if(year){
        let getAwayStats = 'SELECT ps.*, p.player_id, g.*, tr.*, t.* FROM prem_league_player_stats ps, prem_league_players p, '
                         + 'prem_league_games g, prem_league_transfer tr, prem_league_teams t, prem_league_season_dates s '
                         + 'WHERE s.year = ? AND g.game_time >= s.start_date AND g.game_time <= s.end_date AND ps.player_espn_id = p.espn_id '
                         + 'AND ps.game_espn_id = g.espn_id AND tr.id = (SELECT tr1.id FROM prem_league_transfer tr1 WHERE tr1.player_id '
                         + '= p.player_id AND tr1.date_changed = (SELECT MAX(tr2.date_changed) FROM prem_league_transfer tr2 WHERE '
                         + 'tr2.player_id = p.player_id AND tr2.date_changed <= g.game_time)) AND t.team_id = tr.new_team_id AND '
                         + 't.espn_id = g.away_id AND p.player_id = ?';
        let params = [year, player_id];
    }else{
        let getAwayStats = 'SELECT ps.*, p.player_id, g.*, tr.*, t.* FROM prem_league_player_stats ps, prem_league_players p, '
                         + 'prem_league_games g, prem_league_transfer tr, prem_league_teams t WHERE ps.player_espn_id = p.espn_id '
                         + 'AND ps.game_espn_id = g.espn_id AND tr.id = (SELECT tr1.id FROM prem_league_transfer tr1 WHERE tr1.player_id '
                         + '= p.player_id AND tr1.date_changed = (SELECT MAX(tr2.date_changed) FROM prem_league_transfer tr2 WHERE '
                         + 'tr2.player_id = p.player_id AND tr2.date_changed <= g.game_time)) AND t.team_id = tr.new_team_id AND '
                         + 't.espn_id = g.away_id AND p.player_id = ?';
        let params = [player_id];
    }
    return dbHelper.asyncAll(db, getAwayStats, params, true) || [];
}

async function fetchAwayStats(player_id, year){
    let db = openDB();
    if(year){
        let getHomeStats = 'SELECT ps.*, p.player_id, g.*, tr.*, t.* FROM prem_league_player_stats ps, prem_league_players p, '
                         + 'prem_league_games g, prem_league_transfer tr, prem_league_teams t, prem_league_season_dates s '
                         + 'WHERE s.year = ? AND g.game_time >= s.start_date AND g.game_time <= s.end_date AND ps.player_espn_id = p.espn_id '
                         + 'AND ps.game_espn_id = g.espn_id AND tr.id = (SELECT tr1.id FROM prem_league_transfer tr1 WHERE tr1.player_id '
                         + '= p.player_id AND tr1.date_changed = (SELECT MAX(tr2.date_changed) FROM prem_league_transfer tr2 WHERE '
                         + 'tr2.player_id = p.player_id AND tr2.date_changed <= g.game_time)) AND t.team_id = tr.new_team_id AND '
                         + 't.espn_id = g.home_id AND p.player_id = ?';
        let params = [year, player_id];
    }else{
        let getHomeStats = 'SELECT ps.*, p.player_id, g.*, tr.*, t.* FROM prem_league_player_stats ps, prem_league_players p, '
                         + 'prem_league_games g, prem_league_transfer tr, prem_league_teams t WHERE ps.player_espn_id = p.espn_id '
                         + 'AND ps.game_espn_id = g.espn_id AND tr.id = (SELECT tr1.id FROM prem_league_transfer tr1 WHERE tr1.player_id '
                         + '= p.player_id AND tr1.date_changed = (SELECT MAX(tr2.date_changed) FROM prem_league_transfer tr2 WHERE '
                         + 'tr2.player_id = p.player_id AND tr2.date_changed <= g.game_time)) AND t.team_id = tr.new_team_id AND '
                         + 't.espn_id = g.home_id AND p.player_id = ?';
        let params = [player_id];
    }
    return dbHelper.asyncAll(db, getHomeStats, params, true) || [];
}

async function fetchPlayerStatsSingleGame(espn_game_id, espn_player_id){
    let db = openDB();
    let gameStats = 'SELECT * FROM prem_league_player_stats WHERE game_espn_id = ? AND player_espn_id = ? LIMIT 1';
    return await dbHelper.asyncGet(db, gameStats, [espn_game_id, espn_player_id], true);
}

async function updatePlayerSingleGameStats(player){
    let db = openDB();
    let updatePlayer = 'UPDATE prem_league_player_stats SET start = ?, sub = ?, goals = ?, assists = ?, saves = ?, shots = ?, shots_on_target = ?, fouls = ?, fouled = ?, ' + 
                       'offsides = ?, yellow_cards = ?, red_card = ?, team_espn_id = ?, sub_time = ? WHERE game_espn_id = ? AND player_espn_id = ?';
    return await dbHelper.asyncRun(db, updatePlayer, [player['start'], player['sub'], player['goals'], player['assists'], player['saves'], player['shots'], player['shots_on_target'],
                                                      player['fouls'], player['fouled'], player['offsides'], player['yellow_cards'], player['red_cards'], player['team_espn_id'], 
                                                      player['sub_time'], player['game_id'], player['id']], true);
}

async function savePlayerSingleGameStats(player){
    let db = openDB();
    let insertPlayer = 'INSERT INTO prem_league_player_stats (id, game_espn_id, player_espn_id, start, sub, goals, assists, saves, shots, shots_on_target, fouls, fouled, ' + 
                       'offsides, yellow_cards, red_card, team_espn_id, sub_time) VALUES (NULL, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)';
    return await dbHelper.asyncRun(db, insertPlayer, [player['game_id'], player['id'], player['start'], player['sub'], player['goals'], player['assists'], player['saves'], 
                                                      player['shots'], player['shots_on_target'], player['fouls'], player['fouled'], player['offsides'], player['yellow_cards'], 
                                                      player['red_cards'], player['team_espn_id'], player['sub_time']], true);
}

//Export functions for use
module.exports.FetchAllEspnId = FetchAllEspnId;
module.exports.fetchEspnIdByPlayerId = fetchEspnIdByPlayerId;
module.exports.fetchPlayerStatsForSeason = fetchPlayerStatsForSeason;
module.exports.fetchSingleGamePlayerStats = fetchSingleGamePlayerStats;
module.exports.fetchAllPlayerTransfers = fetchAllPlayerTransfers;
module.exports.fetchActiveTeamOnDate = fetchActiveTeamOnDate;
module.exports.fetchActiveTeamsForSeason = fetchActiveTeamsForSeason;
module.exports.fetchLastFourGames = fetchLastFourGames;
module.exports.fetchHomeStats = fetchHomeStats;
module.exports.fetchAwayStats = fetchAwayStats;
module.exports.fetchPlayerStatsSingleGame = fetchPlayerStatsSingleGame;
module.exports.updatePlayerSingleGameStats = updatePlayerSingleGameStats;
module.exports.savePlayerSingleGameStats = savePlayerSingleGameStats;