$('#games_btn').on('click', function(e){
  var start_date = $('#start_date').val();
  var end_date = $('#end_date').val();
  var valid = true;
  var game_list = [];
  var player_list = [];

  var obj = {
    start_date: start_date,
    end_date: end_date
  };

  e.preventDefault();

  if(!start_date.match(/[0-9]{4}[0-9]{2}[0-9]{2}/)){
    alert('Start Date must match YYYY/MM/DD!');
    valid = false;
  }

  if(!end_date.match(/[0-9]{4}[0-9]{2}[0-9]{2}/)){
    alert('End Date must match YYYY/MM/DD!');
    valid = false;
  }

  if(valid){
    console.log('Run Scrape');
    $.get('/admin/api/scrape_games', obj)
      .done(function(data){
        // console.log(data);

        if(data !== {}){
          if(data.games){
            data.games.forEach((game)=>{
              $('#data-scraped').append(game);
            });
          }

          if(data.players){
            data.players.forEach((player, i)=>{
              if(i % 35 <= 17)
                $('[data-game="' + player.game_id + '"]').find('.home-stats').append(player.row);
              else
                $('[data-game="' + player.game_id + '"]').find('.away-stats').append(player.row);
            });
          }
        }
      });
  }
});