var pythonShell = require('python-shell');
var updatesModel = require('./models/updatesModel');
var moment = require('moment');

async function scrapeTransfers(year){
    let year_info = await updatesModel.premYear(year);
    let players = await updatesModel.allPlayers();
    let organized = {};
    let i = 0;
    let player;

    // console.log(players.length);

    while(i < players.length){
        player = players[i];
        if(player){
            // console.log(i);
            await new Promise((resolve, reject)=>{
                pythonShell.run(__dirname + '/../pythonscripts/playerTransfers.py', {args: [parseInt(player.espn_id), parseInt(year)]}, function (err, results){
                    if(err) 
                        reject(err);
                    else{
                        resolve(results);
                    }
                });
            })
            .then((season_info)=>{
                console.log(player.player_id + ': ' + i + '/' + players.length);
                printInfo(season_info, player, year, year_info);
            });
        }
        i++;
    }
}

/* 
    ESPN Scrape returns (example)
    +----------------------------------------------------+
    |   Team Abv            |       Date                 |
    +----------------------------------------------------+
    |   Everton             |    Jan 18, 2017            |
    +----------------------------------------------------+
    |   Man Utd             |    March 29, 2017          |
    +----------------------------------------------------+
        row[0]                      row[1]
*/

async function printInfo(season_info, player, year, year_info){
    player.transfer_history = await updatesModel.transferHistory(player.player_id);
    if(!player.transfer_history)
        player.transfer_history = [];

    // console.log(player.transfer_history);

    player.transfer_history.forEach((transfer)=>{
        transfer.date_changed = moment(transfer.date_changed, 'MM/DD/YYYY').format('YYYY-MM-DD');
        // console.log('Transfer Date: ' + transfer.date_changed);
    });

    //Row contains a single team a player has played for in a season, along with the date of the first game they played for that team
    season_info.forEach(async (row)=>{
        if(!row){
            console.log('ERROR! No page found for player with id ' + player.player_id);
        }else if(row.match(/ERROR!/)){
            console.error(player.player_id + ': ' + row);
        }else{
            //This spits the row into row[0] = the espn team abreviation (ex. 'Man Utd'); and row[1] = date they first appeared
            row = row.split(' | ');

            //Holds the first appearance date from the espn scrape (either a returned date, or the date for the beginning of the searched season because no appearances for the player were found)
            let espn_appearance_date;
            let insert = true;

            //If row[1] === year, than the player has no played games this season
            if(parseInt(row[1]) === parseInt(year)){
                espn_appearance_date = year_info[0].start_date;
                insert = false;
            }else{
                temp = row[1].replace('\r', '');
                espn_appearance_date = moment(temp, 'MMM D, YYYY').subtract(1, 'Days').format('YYYY-MM-DD');
                insert = true;
            }

            //Grab all formatted info for team found in scrape for comparison to previous team (left side of comparison)
            let team_tran_info = await updatesModel.tranInfo(row[0]) || {team_id:500, team_name: 'Out of League'};
            //Hold data for team prior to team found from scrape if available
            let most_recent_team = await findMostRecent(player.transfer_history, espn_appearance_date);
            let new_transfer_row;

            let transferObj = {
                player_id: player.player_id,
                team_id: team_tran_info.team_id,
                date: espn_appearance_date
            };
            // console.log('Name: ' + player.last_name);
            // console.log('Team: ' + team_tran_info.team_name);

            if(team_tran_info.team_id !== most_recent_team){
                // console.log(transferObj);
                if(!insert){
                    let four_months = moment().subtract(4, 'months').format('YYYY-MM-DD');

                    if(moment(four_months).isSameOrAfter(espn_appearance_date)){
                        new_transfer_row = await updatesModel.insertNewTransfer(transferObj);
                    }
                }else{
                    new_transfer_row = await updatesModel.insertNewTransfer(transferObj);
                }
            }
            return new_transfer_row;
        }
    });
}


function findMostRecent(history, espn_appearance_date){
    let most_recent_team = false;

    if(history.length <= 0){
        return false;
    }else{

        return new Promise((resolve, reject)=>{
            //Find the previous team the player played for prior to first appearance for team in row based on date from transfer table
            history.forEach((player_team, i)=>{
                if(player_team.date_changed < espn_appearance_date){
                    //Store the team_id of the team the player most recently played for before the team found by the scrape (right side of comparison)
                    most_recent_team = player_team.new_team_id;
                }else{
                    if(i === (history.length - 1)){
                        return resolve(most_recent_team);
                    }
                }
            });
        });
    }

    console.error('Failure in function findMostRecent!');
}

// console.log(process.argv[2]);
scrapeTransfers(process.argv[2]);