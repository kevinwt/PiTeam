$(document).ready(function(){
    loginView.render();
});

let loginView = {
    render: function(){
        let view = this;

        $('#login-btn').on('click', function(e){view.login()});
    },

    login: function(e){
        let loginObj = {
            username:$('#username').val(),
            password:$('#password').val()
        };
        console.log(loginObj);

        console.log('POST');
        $.post('/users/login', loginObj)
            .done((result)=>{
                console.log(result);
                if(result.status == 'Success'){
                    console.log(window.location.href);
                    window.location.href = window.location.origin + '/users'
                }else if(result.status == 'Failed'){
                    buildPiAlert(result.message, 'red', 5000);
                }
            })
            .fail(err =>{
                console.log(err.responseText);
                buildPiAlert('ERROR: Failure to log in!', 'red', 5000);
            });
    }

};