$('#added_players').hide();

$("#submit").on('click', function(){
    var valid = true;
    var first_name = $("#first_name").val();
    var last_name = $("#last_name").val();
    var team_id = $("#team_id").val();
    var position = $("#position").val();
    var espn_id = $("#espn_id").val();
    var cost = $("#cost").val();

    if((last_name === null || last_name === undefined) || last_name === ''){
        $("#last_nameWarning").html('Last name is required*');
        valid = false;
    }

    if(!team_id || team_id === ''){
        $("#team_idWarning").html('Team is required*');
        valid = false;
    }

    if(!position || position === ''){
        $("#positionWarning").html('Position is required*');
        valid = false;
    }

    var obj = {
        first_name: first_name || '',
        last_name: last_name,
        team_id: team_id,
        position: position,
        espn_id: espn_id || '',
        cost: cost || ''
    };

    if(valid){
        $.get('api/add_player', obj)
        .done(function(player_row){
            $("#added_players").append(player_row);
            $("#added_players").show();
            $("#first_name").val('');
            $("#last_name").val('');
            $("#espn_id").val('');
            $("#cost").val('');
        });
    }

});

$("#search_btn").on('click', function(){
    var obj = {};
    var name = $('#player_search').val().split(' ');
    obj.first_name = name[0];
    obj.last_name = name[1];
    $.get('api/player_search', obj)
    .done(function(html){
        $('#search_table').html(html);
    });
});