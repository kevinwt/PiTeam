$(document).ready(function(){
    adminDashView.render();
});

let adminDashView = {

    render: function(){
        let view = this;

        $('#new-user-btn').on('click', function(e){
            if(!$('#edit-user-panel').length || $('#edit-user-panel').data('user-id') !== 0)
                view.editUser(e);
            else
                piAlert('Already editing user!', 'red', 5000);
        });

        let usersColumns = [
            {
                title: 'Username',
                data: 'username',
            },
            {
                title: 'Access Level',
                data: 'key_name',
            },
            {
                title: 'Email',
                data: 'email',
                render:function(data, type, row, meta){
                    let html = '<div style="width:100px;text-overflow:ellipsis;overflow:hidden;" >'+ data + '</data>';
                    return html;
                }
            },
            {
                title: 'Controls',
                data: '',
                render: function(data, type, row, meta){
                    let html = '<div style="display:flex;justify-content:space-around">';
                    html += '<button class="btn pi-default-btn pi-btn-sm edit-user-btn" data-user-id="' + row.id + '">Edit</button>';
                    html += '<button class="btn pi-default-btn pi-btn-sm delete-user-btn" data-user-id="' + row.id + '">Delete</button>';
                    html += '</div>';
                    return html;
                }
            }
        ];

        view.usersTable = $('#admin-users-table').DataTable({
            columns: usersColumns,
            ajax:{
                url:'/admin/dashboard/api/all_users',
                type: 'GET',
                dataSrc:''
            },
            "bProcessing":true,
            dom: 't',
            deferRender: true,
            pageLength: -1,
            destroy: true,
            // scrollX:true,
            drawCallback:function(){
                $('.delete-user-btn').each((i, btn)=>{
                    console.log($(btn));
                    $(btn).on('click', function(e){
                        view.deleteUser(e);
                        // console.log('Delete User: ' + $(e.currentTarget).data('user-id'));
                    });
                });
    
                $('.edit-user-btn').each((i, btn)=>{
                    console.log($(btn));
                    $(btn).on('click', function(e){
                        view.editUser(e);
                        // console.log('Edit User: ' + $(e.currentTarget).data('user-id'));
                    });
                });
            }
        });
    },

    editUser: function(e){
        let view = this;
        let param = 'edit';
        let id = $(e.currentTarget).data('user-id');
        if(id === 0)
            param = 'new';
        $.post('/admin/dashboard/view/user/' + param, {id:id})
            .done((editTmpl)=>{
                $('.admin-user-display-container').html(editTmpl);
                $('#save-user-btn').on('click', function(e){view.saveUser(e);});
                $('#cancel-user-btn').on('click', function(e){view.cancelUser(e);});
            })
            .fail((err)=>{
                console.error(err.responseText);
            });
    },

    saveUser: function(e){
        let view = this;
        let save = true;
        let userInfo = {
            id:$('#edit-user-panel').data('user-id')
        };
        $('#edit-user-panel [data-field]').each((i, input)=>{
            if($(input).data('field') === 'password'){
                if($(input).data('user-id') === 0 && $(input).val() === ''){
                    save = false;
                    piAlert('All fields must be completed!', 'red', 5000);
                }
            }else if(!$(input).val() || $(input).val() === ''){
                save = false;
                piAlert('All fields must be completed!', 'red', 5000);
            }
            userInfo[$(input).data('field')] = $(input).val();
        });

        if(save){
            $.post('/admin/dashboard/api/save_user', userInfo)
                .done((status)=>{
                    piAlert(status.text, status.color, 5000);
                    view.usersTable.ajax.reload().draw();
                    $('#edit-user-panel').remove();
                });
        }
    },

    cancelUser: function(e){
        $('#edit-user-panel').remove();
    },

    deleteUser: function(e){
        console.log('Deleting User.');
        let view = this;
        let btn = $(e.currentTarget);
        $.post('/admin/dashboard/api/delete_user', {id:btn.data('user-id')})
            .done((status)=>{
                piAlert(status.text, status.color, 5000);
                view.usersTable.ajax.reload().draw();
            });
    }
}