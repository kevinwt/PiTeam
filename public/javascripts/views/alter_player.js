var saveType = 'off';

$(document).ready(function(){
    var scroll = $(window).scrollTop();
    var change = 70 - scroll;
    if(change > 0){
        $('.admin-control-panel-menu').css({'top' : change});
    }else{
        $('.admin-control-panel-menu').css({'top' : '0'});
    }
});

$('#admin-control-add').on('click', function(e){
    var pos_dropdown = '<select id="new-player-position" class="new-player-attr">';
        pos_dropdown += '<option value="GK">GK</option>';
        pos_dropdown += '<option value="DEF">DEF</option>';
        pos_dropdown += '<option value="MID">MID</option>';
        pos_dropdown += '<option value="FOR">FOR</option>';
        pos_dropdown += '</select>';

    $('#admin-table-id').html('');
    $.get('/admin/api/teams_dropdown', {classOpt: "new-player-attr", idOpt: "new-player-team"}).done(function(team_dropdown){

        $('#admin-table-first').html('<input type="text" id="new-player-first" class="new-player-attr">');
        $('#admin-table-last').html('<input type="text" id="new-player-last" class="new-player-attr">');
        $('#admin-table-team').html(team_dropdown);
        $('#admin-table-position').html(pos_dropdown);
        $('#admin-table-cost').html('<input type="text" id="new-player-cost" class="new-player-attr">');
        $('#admin-table-espn').html('<input type="text" id="new-player-espn" class="new-player-attr">');
        $('#admin-table-injured').html('<input type="checkbox" id="new-player-injured" class="new-player-attr">');
        $('#admin-table-date').html('<input type="text" id="new-player-date" class="new-player-attr">');
        $('#admin-add-player-btn').disabled = true;
        $('#new-player-date').val(moment().format('MM/DD/YYYY').toString());
        saveType = 'new';
    });
});

$('#admin-control-cancel').on('click', function(){
    clearMenu();
    saveType = 'off';
});

$('#admin-control-save').on('click', function(e){
    if(saveType === 'new'){
        var curPlayer = {
            first: $('#new-player-first').val(),
            last: $('#new-player-last').val(),
            team: $('#new-player-team').val(),
            cost: $('#new-player-cost').val(),
            position: $('#new-player-position').val(),
            espn: $('#new-player-espn').val(),
            injured: $('#new-player-injured').prop('checked'),
            date: $('#new-player-date').val(),
        };

        $.get('/admin/api/add_player', curPlayer)
        .done(function(newPlayer){
            $('td[data-team='+ newPlayer.team_id + ']').last().parent().after(newPlayer.row);
            playerSelected(newPlayer.raw);
        });

    }else if(saveType === 'edit'){
        var curPlayer = {
            player_id: $('#admin-table-id').html(),
            first: $('#upd-player-first').val(),
            last: $('#upd-player-last').val(),
            team: $('#upd-player-team').val(),
            cost: $('#upd-player-cost').val(),
            position: $('#upd-player-position').val(),
            espn: $('#upd-player-espn').val(),
            injured: $('#upd-player-injured').prop('checked'),
            date: $('#upd-player-date').val(),
        };

        $.post('/admin/api/upd_player', curPlayer)
        .done(function(data){
            $('tr[data-id='+ curPlayer.player_id + ']').addClass('updated').html(data.row);
            playerSelected(data.raw);
        });
    }
});

$('#admin-control-edit').on('click', function(e){
    if(saveType !== 'edit'){
        var posObj = {
            "GK" : '',
            "DEF" : '',
            "MID" : '',
            "FOR" : ''
        };

        if(($('#admin-table-id').html() !== '' && $('#admin-table-id').html() !== null) && $('#admin-table-id').html() !== undefined){
            var curPlayer = {
                id: $('#admin-table-id').html(),
                first: $('#admin-table-first').html(),
                last: $('#admin-table-last').html(),
                team: $('#admin-table-team').html(),
                team_id: $('#admin-table-team').data('team'),
                cost: $('#admin-table-cost').html(),
                position: $('#admin-table-position').html(),
                espn: $('#admin-table-espn').html(),
            };

            posObj[curPlayer.position] = 'selected="selected"';

            if($('#admin-table-injured').html() === 'Yes')
                curPlayer.injured = true;
            else
                curPlayer.injured = false;

            var pos_dropdown = '<select id="upd-player-position" class="new-player-attr">';
                pos_dropdown += '<option value="GK" ' + posObj["GK"] + '>GK</option>';
                pos_dropdown += '<option value="DEF" ' + posObj["DEF"] + '>DEF</option>';
                pos_dropdown += '<option value="MID" ' + posObj["MID"] + '>MID</option>';
                pos_dropdown += '<option value="FOR" ' + posObj["FOR"] + '>FOR</option>';
                pos_dropdown += '</select>';

            $.get('/admin/api/teams_dropdown', {classOpt: "new-player-attr", idOpt: "upd-player-team", defaultOpt: curPlayer.team_id}).done(function(team_dropdown){

                console.log(curPlayer);
                $('#admin-table-first').html('<input type="text" id="upd-player-first" class="new-player-attr" value="' +curPlayer.first + '">');
                $('#admin-table-last').html('<input type="text" id="upd-player-last" class="new-player-attr" value="' +curPlayer.last + '">');
                $('#admin-table-team').html(team_dropdown);
                $('#admin-table-position').html(pos_dropdown);
                $('#admin-table-cost').html('<input type="text" id="upd-player-cost" class="new-player-attr" value="' +curPlayer.cost + '">');
                $('#admin-table-espn').html('<input type="text" id="upd-player-espn" class="new-player-attr" value="' +curPlayer.espn + '">');

                if(curPlayer.injured)
                    $('#admin-table-injured').html('<input type="checkbox" id="upd-player-injured" class="new-player-attr" checked>');
                else
                    $('#admin-table-injured').html('<input type="checkbox" id="upd-player-injured" class="new-player-attr">');

                $('#admin-table-date').html('<input type="text" id="upd-player-date" class="new-player-attr">');
                $('#upd-player-date').val(moment().format('MM/DD/YYYY').toString());
                $('#admin-add-player-btn').disabled = false;
                saveType = 'edit';
            });
        }
    }
});

$('.admin-player-row').on('click', function(e){
    if($('.selected').length > 0)
        $('.selected').removeClass('selected');
    
    var row = $(e.currentTarget);
    row.addClass('selected');
    var player_obj = {
        player_id: row.data('id'),
        espn_id: row.data('espn'),
        first: row.find('.player-first').text(),
        last: row.find('.player-last').text(),
        team: row.find('.player-team').text(),
        team_id: row.find('.player-team').data('team'),
        position: row.find('.player-position').text(),
        cost: row.find('.player-cost').text(),
        injured: row.find('.player-injured').text(),
    };

    playerSelected(player_obj);
    saveType = 'off';
});

$(window).scroll(function(){
    var scroll = $(window).scrollTop();
    var change = 70 - scroll;
    if(change > 0){
        $('.admin-control-panel-menu').css({'top' : change});
    }else{
        $('.admin-control-panel-menu').css({'top' : '0'});
    }
});

function clearMenu(){
    $('#admin-table-id').html('');
    $('#admin-table-first').html('');
    $('#admin-table-last').html('');
    $('#admin-table-team').html('');
    $('#admin-table-position').html('');
    $('#admin-table-cost').html('');
    $('#admin-table-espn').html('');
    $('#admin-table-injured').html('');
    $('#admin-table-date').html('');
    $('#admin-add-player-btn').disabled = false;
}

function playerSelected(player){
    $('#admin-table-id').html(player.player_id);
    $('#admin-table-first').html(player.first);
    $('#admin-table-last').html(player.last);
    $('#admin-table-team').html(player.team);
    $('#admin-table-position').html(player.position);
    $('#admin-table-cost').html(player.cost);
    $('#admin-table-espn').html(player.espn_id);

    if(player.injured === 1 || player.injured === '1')
        $('#admin-table-injured').html('Yes');
    else
        $('#admin-table-injured').html('No');
    $('#admin-table-date').html('');
    $('#admin-table-team').data('team', player.team_id);
}