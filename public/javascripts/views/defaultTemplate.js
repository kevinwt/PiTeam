$(document).ready(function(){
    defaultView.render();
});

let defaultView = {
    render: function(){
        $('.pi-mobile-menu-btn-container').on('click', function(e){
            let icon = $('.pi-mobile-menu-btn-container').find('i');
            icon.toggleClass('fa-bars').toggleClass('fa-ellipsis-v');

            icon.hasClass('fa-bars') ? $('.pi-nav-bar-container').hide() : $('.pi-nav-bar-container').show();
        });
    }
}