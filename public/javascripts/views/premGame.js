$(document).ready(function(){
    let espn_id = $('#game-header-table').data('espn-id');
    let home_id = $('.home-team').data('home-id');
    let away_id = $('.away-team').data('away-id');
    let baseMultiPieSetup = function(renderLocation, caption, label, homeTeam, awayTeam){
        return {
            type: 'multilevelpie',
            width: '100%', // Width of the chart
            height: '400', // Height of the chart
            dataFormat: 'json', // Data type
            renderAt: renderLocation,
            dataSource:{
                "chart": {
                    "caption": caption,
                    "subcaption": homeTeam + " v " + awayTeam,
                    "showBorder": "0",
                    "hoverfillcolor": "#313131",
                    "baseFontColor":"#ffffff",
                    bgColor:"#404040",
                    bgAlpha:"100",
                    "tooltipBgColor":"#404040",
                    "plottoolText":"$value",
                },
                "category": [
                    {
                        "label": label,
                        "color": "#404040",
                        "category": [
                            {
                                "label":homeTeam,
                                "color": "#ff0000",
                                "category": []
                            },
                            {
                                "label":awayTeam,
                                "color": "#0004ff",
                                "category": []
                            }
                        ]
                    }
                ]
            }
        }
    };
    let playerColumns = [
        {
            "data":"last_name",
            "title":"Name"
        },
        {
            "data": "goals",
            "title": "G"
        },
        {
            "data": "assists",
            "title": "A"
        },
        {
            "data": "saves",
            "title": "Sv"
        },
        {
            "data": "shots",
            "title": "Sts"
        },
        {
            "data": "shots_on_target",
            "title": "T"
        },
        {
            "data": "fouls",
            "title": "Fld"
        },
        {
            "data": "fouled",
            "title": "Fls"
        },
    ];

    $.get('/players/single_game_stats/api/' + espn_id, {homeId: home_id, awayId: away_id})
    .done(async function(data){
        // console.log(data);
        // $('#player-stats-container').html(data);
        $('#home-players-table').DataTable({
            columns: playerColumns,
            data: data.homeTeam.players,
            "bProcessing":true,
            dom: 't',
            deferRender: true,
            pageLength: -1,
            destroy: true,
            order:[[2, "asc"]]
        });

        $('#away-players-table').DataTable({
            columns: playerColumns,
            data: data.awayTeam.players,
            "bProcessing":true,
            dom: 't',
            deferRender: true,
            pageLength: -1,
            destroy: true,
            order:[[2, "asc"]]
        });

        let chartData = {
            goals: baseMultiPieSetup("gameGoalsChart", "Goals By Team", "Goals", data.homeTeam.teamName, data.awayTeam.teamName),
            assists: baseMultiPieSetup("gameAssistsChart", "Assists By Team", "Assists", data.homeTeam.teamName, data.awayTeam.teamName),
            saves: baseMultiPieSetup("gameSavesChart", "Saves By Team", "Saves", data.homeTeam.teamName, data.awayTeam.teamName),
            shots: baseMultiPieSetup("gameShotsChart", "Shots By Team", "Shots", data.homeTeam.teamName, data.awayTeam.teamName),
            shots_on_target: baseMultiPieSetup("gameOnTargetChart", "Shots on Target By Team", "Shots on Target", data.homeTeam.teamName, data.awayTeam.teamName),
            fouls: baseMultiPieSetup("gameFoulsChart", "Fouls By Team", "Fouls", data.homeTeam.teamName, data.awayTeam.teamName),
            fouled: baseMultiPieSetup("gameFouledChart", "Fouled By Team", "Fouled", data.homeTeam.teamName, data.awayTeam.teamName),
        };

        let teams = [data.homeTeam.players, data.awayTeam.players];
        await buildGameCharts(teams, chartData);

        for(attr in chartData){
            new FusionCharts(chartData[attr]).render();
        }



        $('.player-details-btn').on('click', function(e){
            let btn = $(e.currentTarget);
            let player_id = btn.parent().parent().data('player-id');
            alert(player_id);
        });
    });

    /* $.get('/players/single_game_stats/api/' + espn_id, {homeId: home_id, awayId: away_id})
    .done(async function(data){
        let chartData = {
            goals: baseMultiPieSetup("gameGoalsChart", "Goals By Team", "Goals", data.homeTeam, data.awayTeam),
            assists: baseMultiPieSetup("gameAssistsChart", "Assists By Team", "Assists", data.homeTeam, data.awayTeam),
            saves: baseMultiPieSetup("gameSavesChart", "Saves By Team", "Saves", data.homeTeam, data.awayTeam),
            shots: baseMultiPieSetup("gameShotsChart", "Shots By Team", "Shots", data.homeTeam, data.awayTeam),
            shots_on_target: baseMultiPieSetup("gameOnTargetChart", "Shots on Target By Team", "Shots on Target", data.homeTeam, data.awayTeam),
            fouls: baseMultiPieSetup("gameFoulsChart", "Fouls By Team", "Fouls", data.homeTeam, data.awayTeam),
            fouled: baseMultiPieSetup("gameFouledChart", "Fouled By Team", "Fouled", data.homeTeam, data.awayTeam),
        };

        let teams = [data.homePlayers, data.awayPlayers];
        await buildGameCharts(teams, chartData);

        for(attr in chartData){
            new FusionCharts(chartData[attr]).render();
        }
    }); */

    $('.player-details-btn').on('click', function(e){
        let player_id = $(e.currentTarget).parent().parent().data('player-id');

        $.get('/players/player_game_details/view/' + player_id)
        .done((tmpl)=>{
            
        });
    });
});

function buildGameCharts(teams, chartData){
    let teamColors = ["#ff0000", "#0004ff"];
    let gameSums = {
        goals:0,
        assists:0,
        saves:0,
        shots:0,
        shots_on_target:0,
        fouls:0,
        fouled:0
    };
    teams.forEach(function(teamArr, i){
        let teamSums = {
            goals:0,
            assists:0,
            saves:0,
            shots:0,
            shots_on_target:0,
            fouls:0,
            fouled:0
        };
        teamArr.forEach(function(player, index){
            if(parseInt(player.goals) > 0){
                teamSums.goals += player.goals;
                gameSums.goals += player.goals;
                chartData.goals["dataSource"]["category"][0]["category"][i]["category"].push(buildPlayerObj(player.last_name, teamColors[i], player.goals));
            }
            if(parseInt(player.assists) > 0){
                teamSums.assists += player.assists;
                gameSums.assists += player.assists;
                chartData.assists["dataSource"]["category"][0]["category"][i]["category"].push(buildPlayerObj(player.last_name, teamColors[i], player.assists));
            }
            if(parseInt(player.saves) > 0){
                teamSums.saves += player.saves;
                gameSums.saves += player.saves;
                chartData.saves["dataSource"]["category"][0]["category"][i]["category"].push(buildPlayerObj(player.last_name, teamColors[i], player.saves));
            }
            if(parseInt(player.shots) > 0){
                teamSums.shots += player.shots;
                gameSums.shots += player.shots;
                chartData.shots["dataSource"]["category"][0]["category"][i]["category"].push(buildPlayerObj(player.last_name, teamColors[i], player.shots));
            }
            if(parseInt(player.shots_on_target) > 0){
                teamSums.shots_on_target += player.shots_on_target;
                gameSums.shots_on_target += player.shots_on_target;
                chartData.shots_on_target["dataSource"]["category"][0]["category"][i]["category"].push(buildPlayerObj(player.last_name, teamColors[i], player.shots_on_target));
            }
            if(parseInt(player.fouls) > 0){
                teamSums.fouls += player.fouls;
                gameSums.fouls += player.fouls;
                chartData.fouls["dataSource"]["category"][0]["category"][i]["category"].push(buildPlayerObj(player.last_name, teamColors[i], player.fouls));
            }
            if(parseInt(player.fouled) > 0){
                teamSums.fouled += player.fouled;
                gameSums.fouled += player.fouled;
                chartData.fouled["dataSource"]["category"][0]["category"][i]["category"].push(buildPlayerObj(player.last_name, teamColors[i], player.fouled));
            }
            
            if(index === (teamArr.length - 1)){
                for(attr in chartData){
                    chartData[attr]["dataSource"]["category"][0]["category"][i]["value"] = teamSums[attr];
                    chartData[attr]["dataSource"]["category"]["value"] = gameSums[attr];
                }
            }
        });
    });
};

function buildPlayerObj(label, color, value){
    return {
        "label" : label,
        "color" : color,
        "value" : value
    };
};