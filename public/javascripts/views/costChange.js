$('.edit-cost-btn').on('click', function(e){
    var btn = $(e.currentTarget);
    var data_id = btn.parent().data('id');
    var row = $('.plyr-row-' + data_id);
    row.toggleClass('price-edit-hide');

    if(row.hasClass('price-edit-hide')){
        btn.html('<span class="glyphicon glyphicon-menu-down"></span>');
    }else{
        btn.html('<span class="glyphicon glyphicon-menu-up"></span>');
    }
});


$('.cost-save-btn').on('click', function(e){
    var id = $(e.currentTarget).parent().parent().data('player');
    var new_cost = $('#cost_' + id).val();
    var new_date = $('#date_' + id).val();
    var cost_obj = {
        player_id: id,
        cost:new_cost,
        date:new_date
    };
    
    $.get('api/update_cost', cost_obj)
        .then(function(response){
            // console.log(response);
            if(response.valid){
                $('tr[data-id=' + id + ']').html(response.html);
                $('#cost_' + id).val('');
                $('#date_' + id).val('');
                $('tr[data-player=' + id + ']').addClass('price-edit-hide');
                $('tr[data-player=' + id + ']').find('.glyphicon-menu-up').removeClass('glyphicon-menu-up').addClass('glyphicon-menu-down');
            }else{
                alert('ERROR! Failed to update!');
            }
        });
});

$('.hide-team-btn').on('click', function(e){
    var btn = $(e.currentTarget);

    $('.team-' + btn.data('team')).each(function(i, row){
        // console.log(row);
        row = $(row);
        row.toggleClass('price-edit-hide');
        // if(row.hasClass('price-edit-hide'))
        //     row.removeClass('price-edit-hide');
        // else
        //     row.addClass('price-edit-hide');
    });
});