$(document).ready(function(){
    fantasyTeamView.render();
});

let fantasyTeamView = {

    render: function(){
        let view = this;

        if($('[data-active-player]').length < 11){

            let premPlayersTableColumns = [
                {
                    data:'name',
                    title:'Name',
                    width:'350px'
                },
                {
                    data:'team_abv',
                    title:'Team',
                    width:'90px'
                },
                {
                    data:'position',
                    title:'Pos',
                    width:'90px'
                },
                // {
                //     data:'points',
                //     title:'Pts'
                // },
                {
                    data:'cost',
                    title:'Cost',
                    render:function(data, type, row){
                        return data ? '$' + parseFloat(data).toFixed(1).toString() + ' mil' : '';
                    },
                    width:'90px'
                },
                {
                    data:'player_id',
                    title:'Add Player',
                    render:function(data, type, row){
                        return '<div style="display:flex;justify-content:center;align-items:center;"><button class="btn add-player pi-default-btn pi-btn-sm" data-player-id="' + data + '">Add Player</button></div>';
                    },
                    width:'100px'
                }
            ];
            let userPlayersTableColumns = [
                {
                    data:'name',
                    title:'Name',
                    width:'350px'
                },
                {
                    data:'team_abv',
                    title:'Team',
                    width:'90px'
                },
                {
                    data:'position',
                    title:'Pos',
                    width:'90px'
                },
                // {
                //     data:'points',
                //     title:'Pts'
                // },
                {
                    data:'cost',
                    title:'Cost',
                    render:function(data, type, row){
                        return data ? '$' + parseFloat(data).toFixed(1).toString() + ' mil' : '';
                    },
                    width:'90px'
                },
                {
                    data:'player_id',
                    title:'Add Player',
                    render:function(data, type, row){
                        return '<div style="display:flex;justify-content:center;align-items:center;"><button class="btn add-player pi-default-btn pi-btn-sm" data-player-id="' + data + '">Add Player</button></div>';
                    },
                    width:'100px'
                }
            ];
            $.get('/users/view/fantasy_team/select_team')
                .then(tmpl =>{
                    $('#base-modal-lg-content').html(tmpl);
                    $('#base-modal-lg').modal({backdrop: 'static', keyboard: false}).on('shown.bs.modal', function(){
                        $('#select-team-cancel-btn').off().on('click', function(e){view.cancelSelectTeamModal(e);});
                        view.userFantasyTeamTable = $('#select-team-user-table').DataTable({
                            ajax: {
                                url: '/users/api/fantasy_team/user_team',
                                type:'GET',
                                data:{
                                    team_id: $('.view-container-column').data('team-id')
                                },
                                dataSrc:''
                            },
                            columns:userPlayersTableColumns,
                            initComplete:function(){

                            }
                        });
                        view.premPlayersTeamTable = $('#select-team-players-table').DataTable({
                            ajax: {
                                url: '/users/api/fantasy_team/prem_players',
                                type:'GET',
                                dataSrc:''
                            },
                            columns:premPlayersTableColumns,
                            initComplete:function(){
                                $('#select-team-players-table').find('.add-player').each(function(i, btn){
                                    function removeRow(e){
                                        let row = $(e.currentTarget).closest('tr');
                                        let player_id = $(e.currentTarget).data('player-id');
                                        view.premPlayersTeamTable.row(row).remove().draw();
                                        $.post('/users/api/fantasy_team/user_team/add_player', {player:player_id})
                                            .then(function(data){
                                                view.userFantasyTeamTable.row.add(data).draw();
                                                $('#select-team-players-table').find('.add-player:last').off().on('click', function(e){removeRow(e);});
                                            });
                                    }

                                    $(btn).off().on('click', function(e){
                                       removeRow(e);
                                    });
                                });
                            }
                        });
                        $('#select-team-save-btn').prop('disabled', true);
                    }).modal('show');
                });
        }

        $('.transfer-btn').on('click', function(e){
            let trans_id = $(e.currentTarget).closest('tr').data('trans-id');
            $.get('/users/transfer/get_modal', {trans_id: trans_id})
                .then(obj =>{
                    if(obj.tmpl){
                        $('#base-modal-content').html(obj.tmpl);
                        $('#base-modal').on('shown.bs.modal', function(){
                            $('#base-modal').find('.trans-modal-player-block').off().on('click', function(e){ view.toggleTransPlayerList($(e.currentTarget).data('action'))});
                        }).modal('show');
                    }else{
                        buildPiAlert('Failed to load modal!', 'red', 5000);
                    }
                });
        });
    },

    toggleTransPlayerList: function(action){
        let view = this;
        $('.trans-modal-player-block').toggleClass('hide');
        $('.trans-modal-player-table').toggleClass('hide');
        console.log(action);
        view.selectTransPlayer(action);
    },

    selectTransPlayer: function(action){
        $('tr[data-player-id]').on('click', function(e){
            let save = {
                action: action,
                player_id: $(e.currentTarget).data('player-id')
            };
            console.log(save);
        });
    },

    cancelSelectTeamModal: function(e){
        let view = this;
        window.location = '/users/dashboard';
    }
}