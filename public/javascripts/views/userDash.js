$(document).ready(function(){
    userDashView.render();
});

let userDashView = {
    render: function(){
        let view = this;
        $('.add-league').on('click', function(e){view.fantasyLeagueModal()});

        $('.active-league').on('click', function(e){
            let league_id = $(e.currentTarget).data('league-id');
            let team_id = $(e.currentTarget).data('team-id');
            console.log($(e.currentTarget).data('league-id'));
            console.log($(e.currentTarget).data('team-id'));
            console.log(window.location);

            window.location.pathname = '/users/fantasy_team/view_team/' + team_id;
        });

        // $('.transfer-btn').on('click', function(e){view.openTransferModal(e)});
    },

    fantasyLeagueModal: function(){
        let view = this;
        $.get('/users/new_league_modal')
            .done((tmpl)=>{
                $('#base-modal-content').html(tmpl);
                
                $('#base-modal').on('shown.bs.modal', function(){
                    $('.close-modal-x').on('click', function(e){
                        $('#base-modal').modal('hide');
                    });

                    $('#create-league-btn').on('click', function(e){
                        $('#create-league-btn').addClass('active-btn-selector');
                        $('#create-league-content').show();
                        $('#confirm-create-btn').show();

                        $('#join-league-btn').removeClass('active-btn-selector');
                        $('#join-league-content').hide();
                        $('#confirm-join-btn').hide();
                    });

                    $('#join-league-btn').on('click', function(e){
                        $('#join-league-btn').addClass('active-btn-selector');
                        $('#join-league-content').show();
                        $('#confirm-join-btn').show();

                        $('#create-league-btn').removeClass('active-btn-selector');
                        $('#create-league-content').hide();
                        $('#confirm-create-btn').hide();
                    });

                    $('#confirm-join-btn').on('click', function(e){view.joinFantasyLeague()});
                    $('#confirm-create-btn').on('click', function(e){view.createFantasyLeague()});
                }).modal('show');
            });
    },

    joinFantasyLeague: function(){
        let league_name = $('#league-name-search').val();
        let password = $('#league-password-search').val();
        let save = true;

        if(!league_name || league_name === ''){
            buildPiAlert('League Name is required!', 'red', 5000);
            save = false;
        }

        if(!password || password === ''){
            buildPiAlert('Password is required!', 'red', 5000);
            save = false;
        }else if(!password.match(/(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,14}$/)){
            buildPiAlert('Credentials do not match any league!', 'red', 5000);
            save = false;
        }

        if(save){
            $.post('/users/league/join', {league_name: league_name, password:password})
                .done((result)=>{
                    buildPiAlert(result.message, result.color, 5000);
                    if(result.leagueInfo){
                        window.location.href = window.location.origin + '/users/' + result.league_info.id + '/new';
                    }
                });
        }
    },

    createFantasyLeague: function(){
        let league_name = $('#league-name').val();
        let password = $('#league-password').val();
        let password_confirm = $('#league-password-confirm').val();
        let save = true;

        if(!league_name || league_name === ''){
            buildPiAlert('League Name is required!', 'red', 5000);
            save = false;
        }

        if(!password || password === ''){
            buildPiAlert('Password is required!', 'red', 5000);
            save = false;
        }else if(!password.match(/(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,14}$/)){
            buildPiAlert('Password must contain at least: 1 lowercase letter, 1 uppercase letter, 1 number and be 8-14 characters long!', 'red', 5000);
            save = false;
        }

        if(!password_confirm || password_confirm === ''){
            buildPiAlert('Password Confirmation is required!', 'red', 5000);
            save = false;
        }

        if(password !== password_confirm){
            buildPiAlert('Passwords do not match!', 'red', 5000);
            save = false;
        }

        if(save){
            $.post('/users/league/create', {league_name: league_name, password:password})
                .done((result)=>{
                    console.log(result);
                });
        }
    },

    // openTransferModal: function(e){
    //     let view = this;


    //     $.get('/users/transfer/get_modal', {trans_id: $(e.currentTarget).data('trans-id')})
    //         .done(obj => {
    //             if(obj.tmpl){
    //                 console.log(obj.tmpl);
    //                 $('#base-modal-content').html(obj.tmpl);
    //                 $('#base-modal').modal('show');
    //             }else{
    //                 buildPiAlert('Failed to load modal!', 'red', 5000);
    //             }
    //         });
    // },

    // newTransfer: function(e){
    //     let team_id = $(e.currentTarget).closest('[data-team-id]').data('team-id');
    //     let league_id = $(e.currentTarget).closest('[data-league-id]').data('league-id');


    // }
};