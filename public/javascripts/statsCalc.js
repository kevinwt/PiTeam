//Calculates a player Points per some unit
function PPUnit (Points, Unit) {
    if(Unit === 0){
        return 0;
    }else{
        var ppu = Points / Unit;
        return ppu;
    }
}

//Used by taking in player stats to calculate their point total 
function CalcPoints (){
    var Points = 0;

    Points += 2 * player.starts;
    Points += player.subIns;
    Points += 5 * player.goals;
    Points += 3 * player.assists;
    Points += 5 * player.cleanSheets;
    Points += -5 * player.redCards;
    
    if(player.saves % 2 == 0){
        Points += player.saves / 2;
    }else{
        Points += (player.saves - 1) / 2;
    }

    Points += -3 * player.pensMissed;
    Points += 5 * player.pensSaved;

    return Points;
}

//Take input from a scrape (hopefully implemented soon) and build an array of player stats for easy update
function PrepDBEntry(Pos, Starts, SubIns, Goals, Assists, CleanSheets, RedCards, Saves, PensMissed, PensSaved) {
    var player = {
        position: Pos,
        starts: Starts,
        subIns: SubIns,
        goals: Goals,
        assists: Assists,
        cleanSheets: CleanSheets,
        redCards: RedCards,
        saves: Saves,
        pensMissed: PensMissed,
        pensSaved:PensSaved
    };

    player.points = CalcPoints(Starts, SubIns, Goals, Assists, CleanSheets, RedCards, Saves, PensMissed, PensSaved);

    return player;
}



//Export functions for calculations
module.exports.PPUnit = PPUnit;
module.exports.CalcPoints = CalcPoints;
module.exports.PrepDBEntry = PrepDBEntry;