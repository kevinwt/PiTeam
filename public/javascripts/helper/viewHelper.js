function buildPiAlert(text, color, time){
    let colors = ['green', 'red', 'blue'];

    if(colors.indexOf(color) === -1){
        throw 'Invalid color selected! (options: "green", "red", "blue")';
    }else{
        $('#pi-alert').html(text);
        $('#pi-alert').addClass('pi-alert-' + color);

        function hideAlert(){
            $('#pi-alert').removeClass('pi-alert-' + color);
            clearInterval(alertShow);
        }

        let alertShow = setInterval(function(){
            hideAlert();
        }, time);
    }
}

function piAlert(text, color, time){
    try{
        buildPiAlert(text, color, time);
    }
    catch(err){
        console.error(err);
    }
}



// QuickSort Functions
function quickSortAttr(arr, attr, left, right, dir){
    let view = this;
    var len = arr.length,
    pivot,
    partitionIndex;
 
   if(left < right){
     pivot = right;
     partitionIndex = view.partition(arr, pivot, attr, left, right, dir);
     
    //sort left and right
    view.quickSortAttr(arr, attr, left, partitionIndex - 1, dir);
    view.quickSortAttr(arr, attr, partitionIndex + 1, right, dir);
   }
   return arr;
}

function partition(arr, pivot, attr, left, right, dir){
    let view = this;
    var pivotValue = arr[pivot][attr],
        partitionIndex = left;

    if(dir === 'ASC'){
        for(var i = left; i < right; i++){
            if(arr[i][attr] < pivotValue){
                view.swap(arr, i, partitionIndex);
                partitionIndex++;
            }
        }
    }else if(dir === 'DESC'){
        for(var i = left; i < right; i++){
            if(arr[i][attr] > pivotValue){
                view.swap(arr, i, partitionIndex);
                partitionIndex++;
            }
        }
    }

   view.swap(arr, right, partitionIndex);
   return partitionIndex;
}

function swap(arr, i, j){
    var temp = arr[i];
    arr[i] = arr[j];
    arr[j] = temp;
}