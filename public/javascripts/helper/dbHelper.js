const sqlite3 = require('sqlite3').verbose();
var Promise = require('bluebird');

function openDB(dbPath){
    let db = new sqlite3.Database(dbPath, (err) => {
        if (err) {
            console.error(err.message);
        }else{
            // console.log('Connected to the database.');
        }
    });

    return db;
}

function closeDB(db){
    db.close((err) => {
        if (err) {
            console.error(err.message);
        }
        // console.log('Close the prem_league_players database connection.');
    });
}

asyncGet = function (db, query, params_arr, close_db){
    var that = this;
    if(params_arr.length > 0){
        return new Promise(function(resolve, reject){
            db.get(query, params_arr, function(err, row){
                if(err){
                    if(close_db)
                        closeDB(db);
                    reject(err);
                }else{
                    if(close_db)
                        closeDB(db);
                    resolve(row);
                }
            });
        });
    }else{
        return new Promise(function(resolve, reject){
            db.get(query, function(err, row){
                if(err){
                    closeDB(db);
                    reject(err);
                }else{
                    closeDB(db);
                    resolve(row);
                }
            });
        });
    }
}

asyncAll = function (db, query, params_arr, close_db){
    var that = this;
    if(params_arr.length > 0){
        return new Promise(function(resolve, reject){
            db.all(query, params_arr, function(err, rows){
                if(err){
                    if(close_db)
                        closeDB(db);
                    reject(err);
                }else{
                    if(close_db)
                        closeDB(db);
                    resolve(rows);
                }
            });
        });
    }else{
        return new Promise(function(resolve, reject){
            db.all(query, function(err, rows){
                if(err){
                    if(close_db)
                        closeDB(db);
                    reject(err);
                }else{
                    if(close_db)
                        closeDB(db);
                    resolve(rows);
                }
            });
        });
    }
}

asyncRun = function (db, query, params_arr, close_db){
    var that = this;
    // console.log(query);
    // console.log(params_arr);
    if(params_arr.length > 0){
        return new Promise(function(resolve, reject){
            db.run(query, params_arr, function(err, res){
                if(err){
                    if(close_db)
                        closeDB(db);
                    reject(err);
                }else{
                    if(close_db)
                        closeDB(db);
                    resolve(res);
                }
            });
        });
    }else{
        return new Promise(function(resolve, reject){
            db.run(query, function(err, res){
                if(err){
                    if(close_db)
                        closeDB(db);
                    reject(err);
                }else{
                    if(close_db)
                        closeDB(db);
                    resolve(res);
                }
            });
        });
    }
}

module.exports.openDB = openDB;
module.exports.closeDB = closeDB;
module.exports.asyncAll = asyncAll;
module.exports.asyncGet = asyncGet;
module.exports.asyncRun = asyncRun;