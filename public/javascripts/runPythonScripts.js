var pythonShell = require('python-shell');
var moment = require('moment');

// USED
// start_date/end_date format = 'YYYY/MM/DD'
// Must await for functions promise to complete
function scheduleScrape(start_date, end_date){
    let options = {
        mode: 'json',
        pythonOptions: ['-u'],
        args: [start_date, end_date]
    };

    return new Promise((resolve, reject)=>{
        pythonShell.run(__dirname + '/../pythonscripts/scheduleScrape.py', options, function (err, results){
            if(err) 
                return reject(err);
            else{
                return resolve(results);
            }
        });
    });

    /* returns Json in formate of:
    { 
    'Game ID': '513688',
    'Home Score': '-',
    'Home Team': 'Everton',
    'Home ID': 368,
    'Away ID': 395,
    'Away Team': 'Watford',
    'Away Score': '-',
    'Game Time': '2018-12-10 20:00' 
    } */
}

// USED
// Scrapes all player stats of a specific game based on game id
function statScrapeById(game_id){
    let options = {
        mode: 'json',
        pythonOptions: ['-u'],
        args: [game_id]
    };

    return new Promise((resolve, reject)=>{
        pythonShell.run(__dirname + '/../pythonscripts/playerStatScrape.py', options, function (err, results){
            if(err) 
                return reject(err);
            else{
                return resolve(results);
            }
        });
    });
}



module.exports.scheduleScrape = scheduleScrape;
module.exports.statScrapeById = statScrapeById;