let dbHelper = require('./helper/dbHelper');
let moment = require('moment');

async function fixDates(){
    let db = dbHelper.openDB('../../databases/players.db');

    let rows = await dbHelper.asyncAll(db, 'SELECT * FROM prem_league_games', [], false);

    await updateRows(rows);
    dbHelper.closeDB(db);
}

async function updateRows(rows){
    rows.forEach((row)=>{
        let time = moment(row.game_time.toString()).format('YYYY-MM-DD hh:mm a');
        // console.log(row.game_id + ', ' + time);
        dbHelper.asyncRun(db, 'UPDATE prem_league_games SET game_time = ? WHERE game_id = ?', [time, row.game_id], false);
    });
};

fixDates();