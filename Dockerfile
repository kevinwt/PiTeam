FROM node:8.9-alpine
ENV NODE_ENV production
ENV SECRET_KEY="da-DFvcsDs.fisgfhB%c3L4zyf41R.1N3vfhn-slipSDk&io%DDvsLJHUdDs"
WORKDIR /usr/src/app
COPY ["package.json", "package-lock.json*", "npm-shrinkwrap.json*", "./"]
RUN npm install
COPY . .
EXPOSE 3000
CMD npm start